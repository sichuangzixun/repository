package com.sichuang.repository.api;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;


/**
 * 幸福南网之东莞供电公众号对接接口
 * 测试接口：http://test.ewaytec2001.cn/qyh
 *
 * @author xs
 * @date 2018/10/8 9:08
 */
@FeignClient(url = "http://www.ewaytec2001.cn/corp", name = "Ewaytec2001API")
public interface Ewaytec2001API {


    /**
     * 验证用户是否登录
     * 参数名	是否必须	参数说明
     *
     * @param grant_type   是	鉴权类型，默认为：authorization_code
     * @param client_id    是	消费者公钥，应用接入时平台提供
     * @param sign         是	签名 MD5（client_id=8120client_key=32ldo2k189timestamp=2018-10-08 10:30:30）
     * @param timestamp    是	时间戳，格式为：yyyy-MM-dd hh:mm:ss
     *  scope        否	保留参数
     *  redirect_uri 否	CallbackUrl，传入应用的使用地址或者url scheme
     * @param code         是	用户授权码
     * @param source       否	如果我方回调第三方页面时有携带此参数，值为2，必须调用企业号的接口地址；否则调用服务号的接口地址。
     * @return 字段    字段说明	取值说明
     * access_token	访问令牌	调用API接口的凭据，一般小时间为6小时
     * expires	有效时间
     * refresh_token	刷新令牌	用于access_token过期后重新获取的凭据，一般有效时间为3个月
     * uid	当前登陆用户id
     */
    @GetMapping(value = "/webServices/romote/accesstokenService/getAccessToken", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    String getAccessToken(@RequestParam("grant_type") String grant_type, @RequestParam("client_id") String client_id, @RequestParam("sign") String sign
            , @RequestParam("timestamp") String timestamp, @RequestParam("code") String code, @RequestParam("source") int source);

    /**
     * 序号	父级元素	元素名称	约束	类型	描述	取值说明
     * 参数名	参数说明	取值说明
     *
     * @param id           1	int	用户id
     * @param access_token 访问令牌，通过鉴权接口获取
     * @param timestamp    当前时间戳	格式为：yyyy-MM-dd hh:mm:ss
     * @param sign         签名	见签名算法
     * @return 序号    父级名称	元素名称	约束	类型	描述	取值说明
     * user	1		用户
     * user	id	1	String	用户账号
     * user	enterpriseid Int	当前用户企业ID
     * user	enterprisename	1	String	企业名称	用,隔开 包含当前用户的上级企业
     * user	departmentid	?	Int	部门ID
     * user	departmentname	?	String	部门名称	用,隔开 包含当前用户的上级部门
     * user	realname ?	String	真实姓名
     * user	nickname	?	String	员工昵称
     * user	position	?	String	用户职位
     * user	headimg ?	String	用户头像
     * user	loginmode	?	Int	登录方式
     * 帐号登录=0
     * 手机号+服务密码=1
     * 手机号+动态密码=2
     * 手机号+帐户密码=3
     * user	userstatus ?	Int	用户状态
     * 未激活=0
     * 已激活=1
     * 锁定=2
     * 冻结=3
     * 删除=4
     * user	businessdomain	?	Int	归属业务域
     * user	mobile	1	String	手机号码
     */
    @GetMapping(value = "/webServices/romote/userInfoService/getEmployeeInfo", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    String getEmployeeInfo(@RequestParam("id") int id, @RequestParam("sign") String sign
            , @RequestParam("timestamp") String timestamp, @RequestParam("access_token") String access_token);
}
