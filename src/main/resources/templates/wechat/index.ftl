<!DOCTYPE html>
<html>
<head>
    <base href="${basePath}">
    <meta charset="utf-8">
    <title>${weChatTitle}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link rel="stylesheet" href="mobile/css/jquery.mobile-1.4.5.min.css">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link rel="stylesheet" href="${basePath + urls.getForLookupPath('/mobile/css/mobile-style.css')}">
</head>
<body>
<style>
    .category_list {
        float: left;
        width: 25%;
    }
</style>
<div data-role="page" id="searchPage">
    <div data-role="header">
        <form id="searchForm" method="post" action="" class="ui-filterable">
            <div class="ui-field-contain">
                <input type="text" data-type="search" name="articleContent" id="search" placeholder="搜索文章">
            </div>
        </form>

    </div>

    <div data-role="main" class="ui-content">
        <div class="m">
                <a href="${basePath}weChat/article/list?categoryIds=1" class="category_list" data-ajax="false">
                    <img class="icon" src="mobile/css/images/01.jpg" />
                    系统操作
                </a>
                <a href="${basePath}weChat/article/list?categoryIds=2" class="category_list" data-ajax="false">
                    <img class="icon" src="mobile/css/images/02.jpg" />
                    服务场景
                </a>
                <a href="${basePath}weChat/article/list?categoryIds=3" class="category_list" data-ajax="false">
                    <img class="icon" src="mobile/css/images/03.jpg" />
                    专题知识
                </a>
                <a href="${basePath}weChat/article/list?categoryIds=4" class="category_list" data-ajax="false">
                    <img class="icon" src="mobile/css/images/04.jpg" />
                    常见问题
                </a>
                <a href="${basePath}weChat/article/list?categoryIds=5" class="category_list" data-ajax="false">
                    <img class="icon" src="mobile/css/images/05.jpg" />
                    其他
                </a>
        </div>

        <div id="wapper">
            <ul data-role="listview" class="ui-field-contain m" id="article_list">
                <li data-role="divider">最近文章</li>
            </ul>
        </div>
        <div id="page_info" style="display: none"></div>
    </div>
    <div data-role="footer">
        <div data-role="navbar" data-iconpos="left" class="menu">
            <ul>
                <li><a href="${basePath}weChat/article" data-icon="home" data-ajax="false" class="active">主页</a></li>
                <li><a href="${basePath}weChat/category" data-icon="bullets" data-ajax="false">索引</a></li>
                <li><a href="${basePath}weChat/suggest" data-icon="mail" data-ajax="false">反馈</a></li>
            </ul>
        </div>
    </div>
</div>
<!-- jQuery -->
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="mobile/js/jquery.mobile-1.4.5.min.js"></script>
<script src="${basePath + urls.getForLookupPath('/js/function.js')}"></script>
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script>

    var successCode = "${successCode}";
    var basePath = "${basePath}";
    var pageNo = 1;
    var pageCount = 1;

    $(function () {

        $('#search').click(function () {
            window.location.href = basePath + "weChat/article/search";
        });

        scrollPage();
    });

    /**
     * 加载下一页
     * @returns {boolean}
     */
    function scrollPage() {
        if (pageNo > pageCount) {
            $("#page_info").text("已经到底了！");
            $("#page_info").show();
            return false;
        }
        $("#page_info").hide();
        var vPost = "";
        var url = 'weChat/article/get?pageNo=' + pageNo;
        $.post(url, vPost, function (data) {
            responseData(data, basePath);
            if (data.code == successCode) {
                var page = data.page;
                pageCount = page.pageCount;
                if (page.totalRecords == 0) {
                    $("#page_info").text("对不起，没有找到相关文章！");
                    $("#page_info").show();
                    return false;
                }
                for (var i = 0; i < data.data.length; i++) {
                    var text = getArticleHtml(basePath, data.data[i], "");
                    $(text).appendTo('#article_list');
                    $("#article_list").listview("refresh");
                }
                pageNo++;
            }
        });
    }


</script>
</body>
</html>
