package com.sichuang.repository.common;

import com.alibaba.fastjson.JSON;
import com.sichuang.repository.controller.BaseController;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.Map;

/**
 * 统一异常处理拦截器
 *
 * @author xs
 * @date 2018/8/20 16:51
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    public void defaultErrorHandler(HttpServletRequest req, HttpServletResponse res, Exception e) throws Exception {
        String accept = req.getHeader("Accept");
        String requestType = req.getHeader("X-Requested-With");
        String msg = "操作失败！" + e.getMessage();
        boolean ajax = (requestType != null && requestType.equalsIgnoreCase("XMLHttpRequest")) ? true : false;
        if (ajax || accept.contains("json")) {     //ajax请求或者请求端接受json数据
            Map<String, Object> map = MyResponse.getErrorResponse(msg);
            res.setContentType("application/json");
            res.setCharacterEncoding("utf-8");
            res.getWriter().print(JSON.toJSONString(map));
        } else {
            msg = URLEncoder.encode(msg, "UTF-8");
            // 获得用户请求的URI
            String path = req.getRequestURI();
            String url = BaseController.AdminMsgUrl;
            // 微信端错误页面
            if (path.indexOf("/weChat") > -1) {
               url = BaseController.MobileMsgUrl;
            }
            res.sendRedirect(url + "msg=" + msg);
        }

        return;
    }


}

