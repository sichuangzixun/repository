package com.sichuang.repository.service.impl;

import com.sichuang.repository.common.FileUtils;
import com.sichuang.repository.common.MyResponse;
import com.sichuang.repository.common.StringUtil;
import com.sichuang.repository.config.BaseConfig;
import com.sichuang.repository.controller.BaseController;
import com.sichuang.repository.dao.ArticleMapper;
import com.sichuang.repository.dao.base.BArticleMapper;
import com.sichuang.repository.model.entity.Article;
import com.sichuang.repository.model.entity.ArticleExample;
import com.sichuang.repository.model.entity.Category;
import com.sichuang.repository.model.entity.Page;
import com.sichuang.repository.service.IArticleService;
import com.sichuang.repository.service.ICategoryService;
import com.sichuang.repository.service.ISearchKeyWordService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author xs
 * @date 2018/8/21 15:42
 */
@Service
public class ArticleService implements IArticleService{

    @Resource private ArticleMapper articleMapper;
    @Resource private BArticleMapper bArticleMapper;
    @Resource private ICategoryService categoryService;
    @Resource private ISearchKeyWordService searchKeyWordService;

    @Override
    public List<Article> getByCategoryId(int categoryId) {
        ArticleExample example = new ArticleExample();
        example.createCriteria().andIsDeleteEqualTo(false)
                .andCategoryIdEqualTo(categoryId);
        example.setOrderByClause("add_time desc");
        return articleMapper.selectByExample(example);
    }

    @Override
    public ArticleExample getExampleByModel(Article article, List<Integer> topCategoryIdList) {
        ArticleExample example = new ArticleExample();
        ArticleExample.Criteria criteria = example.createCriteria();
        if(article.getIsDelete() != null){
            criteria.andIsDeleteEqualTo(article.getIsDelete());
        }
        if(StringUtil.isEmpty(article.getArticleTitle()) == false){
            //搜索标题
            criteria.andArticleTitleLike("%" + article.getArticleTitle() + "%");
        }
        if(article.getCategoryId() != null){
            criteria.andCategoryIdEqualTo(article.getCategoryId());
        }
        if(article.getTopCategoryId() != null){
            criteria.andTopCategoryIdEqualTo(article.getTopCategoryId());
        }
        if(topCategoryIdList!= null && topCategoryIdList.size() > 0){
            criteria.andTopCategoryIdIn(topCategoryIdList);
        }
        return example;
    }

    @Override
    public Map<String, Object> getByModelAndPage(Article article, List<Integer> topCategoryIdList, Page page) {
        ArticleExample example = getExampleByModel(article, topCategoryIdList);
        example.setPage(page);
        example.setOrderByClause("is_top desc, update_time desc");
        List<Article> articleList = articleMapper.selectByExample(example);
        page.setEnd(page.getBegin() + articleList.size());
        page.setTotalRecords(articleMapper.countByExample(example));
        return MyResponse.getResponse(true, page, articleList);
    }

    @Override
    public Map<String, Object> searchArticle(String keyWord, List<Integer> topCategoryIdList, Page page) {
        if(page.getPageNo() == 1){
            //第一页搜索保存关键字
            searchKeyWordService.save(keyWord);
        }
        List<Article> articleList = bArticleMapper.searchRecord(keyWord, topCategoryIdList, page);
        page.setEnd(page.getBegin() + articleList.size());
        page.setTotalRecords(bArticleMapper.countSearchRecord(keyWord, topCategoryIdList));
        return MyResponse.getResponse(true, page, articleList);
    }

    @Override
    public Map<String, Object> search(Article article, String topCategoryIds, Page page) {
        article.setIsDelete(false);
        List<Integer> topCategoryIdList = StringUtil.parseIdList(topCategoryIds);
        if(StringUtil.isEmpty(article.getArticleContent())){
            return getByModelAndPage(article, topCategoryIdList, page);
        }else{
            return searchArticle(article.getArticleContent(), topCategoryIdList, page);
        }
    }

    @Override
    public Map<String, Object> add(Article article) throws Exception {
        //验证数据
        article = verifyData(article);
        //系统信息
        article.setAddTime(new Date());
        article.setUpdateTime(article.getAddTime());
        article.setIsTop(false);
        article.setIsDelete(false);
        article.setArticleId(null);
        int count = articleMapper.insertSelective(article);
        if(count != 1){
            throw new Exception("未知原因！");
        }
        return MyResponse.getSuccessResponse(article);
    }

    @Override
    public Map<String, Object> update(HttpServletRequest request, Article article, String imgSrc) throws Exception {
        //验证文章
        Article articleInfo = verifyArticle(article.getArticleId());
        //验证数据
        article = verifyData(article);
        article.setUpdateTime(new Date());
        int count = articleMapper.updateByPrimaryKeySelective(article);
        if(count != 1){
            throw new Exception("未知原因！");
        }
        //删除图片
        if(StringUtil.isEmpty(imgSrc) == false){
            String[] images = imgSrc.split(",");
            for(int i =0;i<images.length;i++){
                imgSrc = imgSrc.replace(BaseController.getBasePath(request) + BaseConfig.uploadFolderUrl, "");
                FileUtils.deleteFile(imgSrc);
            }
        }
        return MyResponse.getSuccessResponse(article);
    }

    @Override
    public Article verifyArticle(Integer articleId) throws Exception {
        if(articleId == null || articleId <= 0){
            throw new Exception("文章不存在！");
        }
        Article article = articleMapper.selectByPrimaryKey(articleId);
        if(article == null || article.getIsDelete()){
            throw new Exception("文章不存在！");
        }
        return article;
    }

    @Override
    public Map<String, Object> removeByArticleIds(String articleIds) throws Exception {
        if(StringUtil.isEmpty(articleIds)){
            throw new Exception("请选择文章！");
        }
        List<Integer> articleIdList = StringUtil.parseIdList(articleIds);
        ArticleExample example = new ArticleExample();
        example.createCriteria().andIsDeleteEqualTo(false)
                .andArticleIdIn(articleIdList);
        Article article = new Article();
        article.setIsDelete(true);
        int count = articleMapper.updateByExampleSelective(article, example);
        return MyResponse.getResponse(count);
    }

    @Override
    public Map<String, Object> topByArticleIds(String articleIds) throws Exception {
        if(StringUtil.isEmpty(articleIds)){
            throw new Exception("请选择文章！");
        }
        List<Integer> articleIdList = StringUtil.parseIdList(articleIds);
        ArticleExample example = new ArticleExample();
        example.createCriteria().andIsDeleteEqualTo(false)
                .andArticleIdIn(articleIdList).andIsTopEqualTo(false);
        Article article = new Article();
        article.setIsTop(true);
        int count = articleMapper.updateByExampleSelective(article, example);
        return MyResponse.getResponse(count);
    }

    @Override
    public Article verifyData(Article article) throws Exception {
        //验证字段
        if(StringUtil.isEmpty(article.getArticleTitle())){
            throw new Exception("文章标题不能为空！");
        }
        if(StringUtil.isEmpty(article.getArticleContent())){
            throw new Exception("文章内容不能为空！");
        }
        //验证分类
        Category category = categoryService.verifyCategory(article.getCategoryId());
        article.setCategoryName(category.getCategoryName());
        article.setTopCategoryId(category.getTopCategoryId());
        //生成摘要
        String content = StringUtil.StripHTML(article.getArticleContent());
        int len = content.length();
        if(len > 200){
            content = content.substring(0, 200);
        }
        article.setArticleAbstract(content);

        return article;
    }

    @Override
    public Map<String, Object> updateCategory(List<Integer> categoryIdList) {
        ArticleExample example = new ArticleExample();
        example.createCriteria().andCategoryIdIn(categoryIdList)
                .andIsDeleteEqualTo(false);
        Article article = new Article();
        article.setTopCategoryId(5);
        article.setCategoryId(5);
        article.setCategoryName("其他");
        int count= articleMapper.updateByExampleSelective(article, example);
        return MyResponse.getSQLSuccessResponse();
    }

    @Override
    public Map<String, Object> updateCategoryName(Category category) {
        ArticleExample example = new ArticleExample();
        example.createCriteria().andCategoryIdEqualTo(category.getCategoryId())
                .andIsDeleteEqualTo(false);
        Article article = new Article();
        article.setCategoryName(category.getCategoryName());
        int count= articleMapper.updateByExampleSelective(article, example);
        return MyResponse.getSQLSuccessResponse();
    }
}
