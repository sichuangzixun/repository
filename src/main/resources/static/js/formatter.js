

/**
 * 用户类型：1-高级管理员、2-普通管理员、3-保洁员
 * @param val
 * @returns {*}
 */
function userTypeFormatter(val){
    var type = {1:'高级管理员',2:'普通管理员',3:'保洁员'};
    return type[val];
}

/**
 * 布尔型状态
 * @param val
 * @returns {*}
 */
function booleanFormatter(val) {
    if(val == false){
        return "否";
    }
    if(val == true){
        return "是";
    }
    return val;
}

/**
 * 禁用状态：0禁用 1正常
 * @param val
 * @returns {*}
 */
function disabledFormatter(val) {
    if(val == false){
        return "<span class='text-danger'>已禁用</span>";
    }
    if(val == true){
        return "正常";
    }
    return val;
}