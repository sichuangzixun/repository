<!DOCTYPE html>
<html>
<head>
    <base href="${basePath}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>知识库管理后台系统</title>
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="hplus/css/animate.min.css" rel="stylesheet">
    <link href="hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
</head>
<body class="gray-bg">
<div class="middle-box text-center loginscreen  animated fadeInDown">
    <div>
        <div>
            <h1 class="logo-name"></h1>
        </div>
        <h3>欢迎使用知识库管理后台系统</h3>
        <form class="m-t" role="form" id="userForm">
            <div class="form-group">
                <input type="text" class="form-control" name="loginName" placeholder="用户名" required="">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" id="password" placeholder="密码" required="">
                <input type="hidden" value="" name="password">
            </div>
            <button type="button" id="login" class="btn btn-primary block full-width m-b">登 录</button>
        </form>
    </div>
</div>
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="hplus/js/bootstrap.min.js?v=3.3.6"></script>
<script src="${basePath + urls.getForLookupPath('/js/function.js')}"></script>
<script src="${basePath + urls.getForLookupPath('/js/md5.js')}"></script>
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script type="text/javascript">
    $(function () {
        var successCode = "${successCode}";
        var basePath = "${basePath}";
        $('#login').click(function (e) {
            var vUser = $('#userForm input[name="loginName"]').val();
            var vPwd = $('#password').val();
            if (vUser == '') {
                alert('请填写用户名');
                return false;
            }
            if (vPwd == '') {
                alert('请填写用户密码');
                return false;
            }
            var str = hex_md5(vPwd);
            $('#userForm input[name="password"]').val(str);
            var vPost = $('#userForm').serialize();
            var url = "api/user/login";
            layer.load(0, {shade: false}); //0代表加载的风格，支持0-2
            $.post(url, vPost, function (data) {
                responseData(data, basePath);
                if (data.code == successCode) {
                    top.location.href = basePath;
                }
            });
        });

    });


</script>
</body>
</html>