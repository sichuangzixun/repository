package com.sichuang.repository.service;

import com.sichuang.repository.model.entity.Page;
import com.sichuang.repository.model.entity.Suggest;

import java.util.Map;

/**
 * @author xs
 * @date 2018/8/31 14:18
 */
public interface ISuggestService {

    /**
     * 添加反馈
     * @param suggest
     * @return
     */
    Map<String, Object> add(Suggest suggest) throws Exception;

    /**
     * 回复反馈
     * @param suggest
     * @return
     */
    Map<String, Object> update(Suggest suggest) throws Exception;

    /**
     * 验证反馈意见
     * @param suggestId
     * @return
     */
    Suggest verifySuggest(Integer suggestId) throws Exception;

    /**
     * 获取反馈信息
     * @param page
     * @return
     */
    Map<String, Object> getByPage(Page page, Integer isProcessed);

    /**
     * 批量移除
     * @param suggestIds
     * @return
     */
    Map<String, Object> remove(String suggestIds) throws Exception;
}
