package com.sichuang.repository.common;


import com.sichuang.repository.controller.BaseController;
import com.sichuang.repository.model.entity.User;
import com.sichuang.repository.other.EnumManage;
import com.sichuang.repository.service.IAccessRecordService;
import com.sichuang.repository.service.IUserService;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.Map;

/**
 * 登录过滤器
 *
 * @author qw
 */
public class WeChatLoginFilter implements Filter {

    private IUserService userService;

    @Override
    public void destroy() {
        // TODO Auto-generated method stub

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        // TODO Auto-generated method stub
        // 获得在下面代码中要用的request,response,session对象
        HttpServletRequest servletRequest = (HttpServletRequest) request;
        HttpServletResponse servletResponse = (HttpServletResponse) response;

        // 获得用户请求的URI
        String path = servletRequest.getRequestURI();

        // 获取请求的类型 0：无需登录 1：需要登录
        User userSession = BaseController.getUserSession(servletRequest);

        // 登陆页面和微信端无需过滤
        if (path.indexOf("/login") > -1 || path.indexOf("/message") > -1 || path.indexOf("/weChat") == -1) {
            chain.doFilter(servletRequest, servletResponse);
            return;
        }
        System.out.println(path);

        // 判断如果没有取到用户信息
        if (userSession == null) {
            if (userService == null) {
                BeanFactory factory = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getServletContext());
                userService = (IUserService) factory.getBean("userService");
            }
            try {
                userService.weChatLogin(servletRequest);
            } catch (Exception e) {
                String message = URLEncoder.encode("对不起，您的身份信息验证失败或已过期！", "UTF-8");
                servletResponse.sendRedirect(BaseController.MobileMsgUrl + "msg=" + message);
            }
        }
        // 已经登陆,继续此次请求
        chain.doFilter(request, response);
        return;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // TODO Auto-generated method stub

    }

}
