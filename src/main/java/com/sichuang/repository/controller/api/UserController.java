package com.sichuang.repository.controller.api;

import com.sichuang.repository.controller.BaseController;
import com.sichuang.repository.model.entity.Page;
import com.sichuang.repository.model.entity.User;
import com.sichuang.repository.service.IUserService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/api/user")
public class UserController extends BaseController {

    @Resource
    private IUserService userService;

    //------------------- 用户登录--------------------------------------------------------
    @RequestMapping("/login")
    public Map<String, Object> login(HttpServletRequest request, String loginName, String password) throws Exception {
        User user = new User();
        user.setLoginName(loginName);
        user.setPassword(password);
        return userService.login(request, user);
    }

    @RequestMapping("/changePassword")
    private Map<String, Object> changePassword(HttpServletRequest request, String oldPassword, String newPassword) throws Exception {
        if (!isLogin(request)) {
            return getNotLoginResponse();
        }
        return userService.changePassword(request, oldPassword, newPassword);
    }

    @RequestMapping("/search")
    public Map<String, Object> search(HttpServletRequest request, Page page){
        return userService.search(page);
    }

}
