package com.sichuang.repository.model.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SuggestExample {
    /**   tableName: suggest   **/
    protected String orderByClause;

    /**   tableName: suggest   **/
    protected boolean distinct;

    /**   tableName: suggest   **/
    protected List<Criteria> oredCriteria;

    /**   tableName: suggest   **/
    protected Page page;

    private Integer limit;

    private Integer offset;

    private Boolean forUpdate;

    public SuggestExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setPage(Page page) {
        this.page=page;
    }

    public Page getPage() {
        return page;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setForUpdate(Boolean forUpdate) {
        this.forUpdate = forUpdate;
    }

    public Boolean getForUpdate() {
        return forUpdate;
    }

    /** suggest **/
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andSuggestIdIsNull() {
            addCriterion("suggest_id is null");
            return (Criteria) this;
        }

        public Criteria andSuggestIdIsNotNull() {
            addCriterion("suggest_id is not null");
            return (Criteria) this;
        }

        public Criteria andSuggestIdEqualTo(Integer value) {
            addCriterion("suggest_id =", value, "suggestId");
            return (Criteria) this;
        }

        public Criteria andSuggestIdNotEqualTo(Integer value) {
            addCriterion("suggest_id <>", value, "suggestId");
            return (Criteria) this;
        }

        public Criteria andSuggestIdGreaterThan(Integer value) {
            addCriterion("suggest_id >", value, "suggestId");
            return (Criteria) this;
        }

        public Criteria andSuggestIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("suggest_id >=", value, "suggestId");
            return (Criteria) this;
        }

        public Criteria andSuggestIdLessThan(Integer value) {
            addCriterion("suggest_id <", value, "suggestId");
            return (Criteria) this;
        }

        public Criteria andSuggestIdLessThanOrEqualTo(Integer value) {
            addCriterion("suggest_id <=", value, "suggestId");
            return (Criteria) this;
        }

        public Criteria andSuggestIdIn(List<Integer> values) {
            addCriterion("suggest_id in", values, "suggestId");
            return (Criteria) this;
        }

        public Criteria andSuggestIdNotIn(List<Integer> values) {
            addCriterion("suggest_id not in", values, "suggestId");
            return (Criteria) this;
        }

        public Criteria andSuggestIdBetween(Integer value1, Integer value2) {
            addCriterion("suggest_id between", value1, value2, "suggestId");
            return (Criteria) this;
        }

        public Criteria andSuggestIdNotBetween(Integer value1, Integer value2) {
            addCriterion("suggest_id not between", value1, value2, "suggestId");
            return (Criteria) this;
        }

        public Criteria andSuggestContentIsNull() {
            addCriterion("suggest_content is null");
            return (Criteria) this;
        }

        public Criteria andSuggestContentIsNotNull() {
            addCriterion("suggest_content is not null");
            return (Criteria) this;
        }

        public Criteria andSuggestContentEqualTo(String value) {
            addCriterion("suggest_content =", value, "suggestContent");
            return (Criteria) this;
        }

        public Criteria andSuggestContentNotEqualTo(String value) {
            addCriterion("suggest_content <>", value, "suggestContent");
            return (Criteria) this;
        }

        public Criteria andSuggestContentGreaterThan(String value) {
            addCriterion("suggest_content >", value, "suggestContent");
            return (Criteria) this;
        }

        public Criteria andSuggestContentGreaterThanOrEqualTo(String value) {
            addCriterion("suggest_content >=", value, "suggestContent");
            return (Criteria) this;
        }

        public Criteria andSuggestContentLessThan(String value) {
            addCriterion("suggest_content <", value, "suggestContent");
            return (Criteria) this;
        }

        public Criteria andSuggestContentLessThanOrEqualTo(String value) {
            addCriterion("suggest_content <=", value, "suggestContent");
            return (Criteria) this;
        }

        public Criteria andSuggestContentLike(String value) {
            addCriterion("suggest_content like", value, "suggestContent");
            return (Criteria) this;
        }

        public Criteria andSuggestContentNotLike(String value) {
            addCriterion("suggest_content not like", value, "suggestContent");
            return (Criteria) this;
        }

        public Criteria andSuggestContentIn(List<String> values) {
            addCriterion("suggest_content in", values, "suggestContent");
            return (Criteria) this;
        }

        public Criteria andSuggestContentNotIn(List<String> values) {
            addCriterion("suggest_content not in", values, "suggestContent");
            return (Criteria) this;
        }

        public Criteria andSuggestContentBetween(String value1, String value2) {
            addCriterion("suggest_content between", value1, value2, "suggestContent");
            return (Criteria) this;
        }

        public Criteria andSuggestContentNotBetween(String value1, String value2) {
            addCriterion("suggest_content not between", value1, value2, "suggestContent");
            return (Criteria) this;
        }

        public Criteria andAddTimeIsNull() {
            addCriterion("add_time is null");
            return (Criteria) this;
        }

        public Criteria andAddTimeIsNotNull() {
            addCriterion("add_time is not null");
            return (Criteria) this;
        }

        public Criteria andAddTimeEqualTo(Date value) {
            addCriterion("add_time =", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeNotEqualTo(Date value) {
            addCriterion("add_time <>", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeGreaterThan(Date value) {
            addCriterion("add_time >", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("add_time >=", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeLessThan(Date value) {
            addCriterion("add_time <", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeLessThanOrEqualTo(Date value) {
            addCriterion("add_time <=", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeIn(List<Date> values) {
            addCriterion("add_time in", values, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeNotIn(List<Date> values) {
            addCriterion("add_time not in", values, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeBetween(Date value1, Date value2) {
            addCriterion("add_time between", value1, value2, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeNotBetween(Date value1, Date value2) {
            addCriterion("add_time not between", value1, value2, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddUserNameIsNull() {
            addCriterion("add_user_name is null");
            return (Criteria) this;
        }

        public Criteria andAddUserNameIsNotNull() {
            addCriterion("add_user_name is not null");
            return (Criteria) this;
        }

        public Criteria andAddUserNameEqualTo(String value) {
            addCriterion("add_user_name =", value, "addUserName");
            return (Criteria) this;
        }

        public Criteria andAddUserNameNotEqualTo(String value) {
            addCriterion("add_user_name <>", value, "addUserName");
            return (Criteria) this;
        }

        public Criteria andAddUserNameGreaterThan(String value) {
            addCriterion("add_user_name >", value, "addUserName");
            return (Criteria) this;
        }

        public Criteria andAddUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("add_user_name >=", value, "addUserName");
            return (Criteria) this;
        }

        public Criteria andAddUserNameLessThan(String value) {
            addCriterion("add_user_name <", value, "addUserName");
            return (Criteria) this;
        }

        public Criteria andAddUserNameLessThanOrEqualTo(String value) {
            addCriterion("add_user_name <=", value, "addUserName");
            return (Criteria) this;
        }

        public Criteria andAddUserNameLike(String value) {
            addCriterion("add_user_name like", value, "addUserName");
            return (Criteria) this;
        }

        public Criteria andAddUserNameNotLike(String value) {
            addCriterion("add_user_name not like", value, "addUserName");
            return (Criteria) this;
        }

        public Criteria andAddUserNameIn(List<String> values) {
            addCriterion("add_user_name in", values, "addUserName");
            return (Criteria) this;
        }

        public Criteria andAddUserNameNotIn(List<String> values) {
            addCriterion("add_user_name not in", values, "addUserName");
            return (Criteria) this;
        }

        public Criteria andAddUserNameBetween(String value1, String value2) {
            addCriterion("add_user_name between", value1, value2, "addUserName");
            return (Criteria) this;
        }

        public Criteria andAddUserNameNotBetween(String value1, String value2) {
            addCriterion("add_user_name not between", value1, value2, "addUserName");
            return (Criteria) this;
        }

        public Criteria andAddUserIdIsNull() {
            addCriterion("add_user_id is null");
            return (Criteria) this;
        }

        public Criteria andAddUserIdIsNotNull() {
            addCriterion("add_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andAddUserIdEqualTo(Integer value) {
            addCriterion("add_user_id =", value, "addUserId");
            return (Criteria) this;
        }

        public Criteria andAddUserIdNotEqualTo(Integer value) {
            addCriterion("add_user_id <>", value, "addUserId");
            return (Criteria) this;
        }

        public Criteria andAddUserIdGreaterThan(Integer value) {
            addCriterion("add_user_id >", value, "addUserId");
            return (Criteria) this;
        }

        public Criteria andAddUserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("add_user_id >=", value, "addUserId");
            return (Criteria) this;
        }

        public Criteria andAddUserIdLessThan(Integer value) {
            addCriterion("add_user_id <", value, "addUserId");
            return (Criteria) this;
        }

        public Criteria andAddUserIdLessThanOrEqualTo(Integer value) {
            addCriterion("add_user_id <=", value, "addUserId");
            return (Criteria) this;
        }

        public Criteria andAddUserIdIn(List<Integer> values) {
            addCriterion("add_user_id in", values, "addUserId");
            return (Criteria) this;
        }

        public Criteria andAddUserIdNotIn(List<Integer> values) {
            addCriterion("add_user_id not in", values, "addUserId");
            return (Criteria) this;
        }

        public Criteria andAddUserIdBetween(Integer value1, Integer value2) {
            addCriterion("add_user_id between", value1, value2, "addUserId");
            return (Criteria) this;
        }

        public Criteria andAddUserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("add_user_id not between", value1, value2, "addUserId");
            return (Criteria) this;
        }

        public Criteria andReplyMessageIsNull() {
            addCriterion("reply_message is null");
            return (Criteria) this;
        }

        public Criteria andReplyMessageIsNotNull() {
            addCriterion("reply_message is not null");
            return (Criteria) this;
        }

        public Criteria andReplyMessageEqualTo(String value) {
            addCriterion("reply_message =", value, "replyMessage");
            return (Criteria) this;
        }

        public Criteria andReplyMessageNotEqualTo(String value) {
            addCriterion("reply_message <>", value, "replyMessage");
            return (Criteria) this;
        }

        public Criteria andReplyMessageGreaterThan(String value) {
            addCriterion("reply_message >", value, "replyMessage");
            return (Criteria) this;
        }

        public Criteria andReplyMessageGreaterThanOrEqualTo(String value) {
            addCriterion("reply_message >=", value, "replyMessage");
            return (Criteria) this;
        }

        public Criteria andReplyMessageLessThan(String value) {
            addCriterion("reply_message <", value, "replyMessage");
            return (Criteria) this;
        }

        public Criteria andReplyMessageLessThanOrEqualTo(String value) {
            addCriterion("reply_message <=", value, "replyMessage");
            return (Criteria) this;
        }

        public Criteria andReplyMessageLike(String value) {
            addCriterion("reply_message like", value, "replyMessage");
            return (Criteria) this;
        }

        public Criteria andReplyMessageNotLike(String value) {
            addCriterion("reply_message not like", value, "replyMessage");
            return (Criteria) this;
        }

        public Criteria andReplyMessageIn(List<String> values) {
            addCriterion("reply_message in", values, "replyMessage");
            return (Criteria) this;
        }

        public Criteria andReplyMessageNotIn(List<String> values) {
            addCriterion("reply_message not in", values, "replyMessage");
            return (Criteria) this;
        }

        public Criteria andReplyMessageBetween(String value1, String value2) {
            addCriterion("reply_message between", value1, value2, "replyMessage");
            return (Criteria) this;
        }

        public Criteria andReplyMessageNotBetween(String value1, String value2) {
            addCriterion("reply_message not between", value1, value2, "replyMessage");
            return (Criteria) this;
        }

        public Criteria andReplyTimeIsNull() {
            addCriterion("reply_time is null");
            return (Criteria) this;
        }

        public Criteria andReplyTimeIsNotNull() {
            addCriterion("reply_time is not null");
            return (Criteria) this;
        }

        public Criteria andReplyTimeEqualTo(Date value) {
            addCriterion("reply_time =", value, "replyTime");
            return (Criteria) this;
        }

        public Criteria andReplyTimeNotEqualTo(Date value) {
            addCriterion("reply_time <>", value, "replyTime");
            return (Criteria) this;
        }

        public Criteria andReplyTimeGreaterThan(Date value) {
            addCriterion("reply_time >", value, "replyTime");
            return (Criteria) this;
        }

        public Criteria andReplyTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("reply_time >=", value, "replyTime");
            return (Criteria) this;
        }

        public Criteria andReplyTimeLessThan(Date value) {
            addCriterion("reply_time <", value, "replyTime");
            return (Criteria) this;
        }

        public Criteria andReplyTimeLessThanOrEqualTo(Date value) {
            addCriterion("reply_time <=", value, "replyTime");
            return (Criteria) this;
        }

        public Criteria andReplyTimeIn(List<Date> values) {
            addCriterion("reply_time in", values, "replyTime");
            return (Criteria) this;
        }

        public Criteria andReplyTimeNotIn(List<Date> values) {
            addCriterion("reply_time not in", values, "replyTime");
            return (Criteria) this;
        }

        public Criteria andReplyTimeBetween(Date value1, Date value2) {
            addCriterion("reply_time between", value1, value2, "replyTime");
            return (Criteria) this;
        }

        public Criteria andReplyTimeNotBetween(Date value1, Date value2) {
            addCriterion("reply_time not between", value1, value2, "replyTime");
            return (Criteria) this;
        }

        public Criteria andReplyUserNameIsNull() {
            addCriterion("reply_user_name is null");
            return (Criteria) this;
        }

        public Criteria andReplyUserNameIsNotNull() {
            addCriterion("reply_user_name is not null");
            return (Criteria) this;
        }

        public Criteria andReplyUserNameEqualTo(String value) {
            addCriterion("reply_user_name =", value, "replyUserName");
            return (Criteria) this;
        }

        public Criteria andReplyUserNameNotEqualTo(String value) {
            addCriterion("reply_user_name <>", value, "replyUserName");
            return (Criteria) this;
        }

        public Criteria andReplyUserNameGreaterThan(String value) {
            addCriterion("reply_user_name >", value, "replyUserName");
            return (Criteria) this;
        }

        public Criteria andReplyUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("reply_user_name >=", value, "replyUserName");
            return (Criteria) this;
        }

        public Criteria andReplyUserNameLessThan(String value) {
            addCriterion("reply_user_name <", value, "replyUserName");
            return (Criteria) this;
        }

        public Criteria andReplyUserNameLessThanOrEqualTo(String value) {
            addCriterion("reply_user_name <=", value, "replyUserName");
            return (Criteria) this;
        }

        public Criteria andReplyUserNameLike(String value) {
            addCriterion("reply_user_name like", value, "replyUserName");
            return (Criteria) this;
        }

        public Criteria andReplyUserNameNotLike(String value) {
            addCriterion("reply_user_name not like", value, "replyUserName");
            return (Criteria) this;
        }

        public Criteria andReplyUserNameIn(List<String> values) {
            addCriterion("reply_user_name in", values, "replyUserName");
            return (Criteria) this;
        }

        public Criteria andReplyUserNameNotIn(List<String> values) {
            addCriterion("reply_user_name not in", values, "replyUserName");
            return (Criteria) this;
        }

        public Criteria andReplyUserNameBetween(String value1, String value2) {
            addCriterion("reply_user_name between", value1, value2, "replyUserName");
            return (Criteria) this;
        }

        public Criteria andReplyUserNameNotBetween(String value1, String value2) {
            addCriterion("reply_user_name not between", value1, value2, "replyUserName");
            return (Criteria) this;
        }

        public Criteria andIsDeleteIsNull() {
            addCriterion("is_delete is null");
            return (Criteria) this;
        }

        public Criteria andIsDeleteIsNotNull() {
            addCriterion("is_delete is not null");
            return (Criteria) this;
        }

        public Criteria andIsDeleteEqualTo(Boolean value) {
            addCriterion("is_delete =", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteNotEqualTo(Boolean value) {
            addCriterion("is_delete <>", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteGreaterThan(Boolean value) {
            addCriterion("is_delete >", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_delete >=", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteLessThan(Boolean value) {
            addCriterion("is_delete <", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteLessThanOrEqualTo(Boolean value) {
            addCriterion("is_delete <=", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteIn(List<Boolean> values) {
            addCriterion("is_delete in", values, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteNotIn(List<Boolean> values) {
            addCriterion("is_delete not in", values, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteBetween(Boolean value1, Boolean value2) {
            addCriterion("is_delete between", value1, value2, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_delete not between", value1, value2, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsProcessedIsNull() {
            addCriterion("is_processed is null");
            return (Criteria) this;
        }

        public Criteria andIsProcessedIsNotNull() {
            addCriterion("is_processed is not null");
            return (Criteria) this;
        }

        public Criteria andIsProcessedEqualTo(Integer value) {
            addCriterion("is_processed =", value, "isProcessed");
            return (Criteria) this;
        }

        public Criteria andIsProcessedNotEqualTo(Integer value) {
            addCriterion("is_processed <>", value, "isProcessed");
            return (Criteria) this;
        }

        public Criteria andIsProcessedGreaterThan(Integer value) {
            addCriterion("is_processed >", value, "isProcessed");
            return (Criteria) this;
        }

        public Criteria andIsProcessedGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_processed >=", value, "isProcessed");
            return (Criteria) this;
        }

        public Criteria andIsProcessedLessThan(Integer value) {
            addCriterion("is_processed <", value, "isProcessed");
            return (Criteria) this;
        }

        public Criteria andIsProcessedLessThanOrEqualTo(Integer value) {
            addCriterion("is_processed <=", value, "isProcessed");
            return (Criteria) this;
        }

        public Criteria andIsProcessedIn(List<Integer> values) {
            addCriterion("is_processed in", values, "isProcessed");
            return (Criteria) this;
        }

        public Criteria andIsProcessedNotIn(List<Integer> values) {
            addCriterion("is_processed not in", values, "isProcessed");
            return (Criteria) this;
        }

        public Criteria andIsProcessedBetween(Integer value1, Integer value2) {
            addCriterion("is_processed between", value1, value2, "isProcessed");
            return (Criteria) this;
        }

        public Criteria andIsProcessedNotBetween(Integer value1, Integer value2) {
            addCriterion("is_processed not between", value1, value2, "isProcessed");
            return (Criteria) this;
        }
    }

    /**  tableName: suggest   **/
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /** suggest **/
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}