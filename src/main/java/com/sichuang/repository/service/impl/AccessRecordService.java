package com.sichuang.repository.service.impl;

import com.alibaba.fastjson.JSON;
import com.sichuang.repository.controller.BaseController;
import com.sichuang.repository.dao.AccessRecordMapper;
import com.sichuang.repository.model.entity.AccessRecord;
import com.sichuang.repository.model.entity.User;
import com.sichuang.repository.service.IAccessRecordService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * @author xs
 * @date 2018/9/20 15:51
 */
@Service
public class AccessRecordService implements IAccessRecordService{

    @Resource private AccessRecordMapper accessRecordMapper;

    @Override
    public int add(HttpServletRequest request) {
        AccessRecord accessRecord = new AccessRecord();
        accessRecord.setAccessUrl(request.getRequestURI());
        String param = JSON.toJSONString(request.getParameterMap());
        if(param.length() > 255){
            param = param.substring(0,255);
        }
        accessRecord.setAccessParam(param);
        User userSession = BaseController.getUserSession(request);
        if(userSession != null){
            accessRecord.setAccessUserId(userSession.getUserId());
            accessRecord.setAccessUserName(userSession.getLoginName());
        }
        accessRecord.setAccessTime(new Date());
        return accessRecordMapper.insertSelective(accessRecord);
    }
}
