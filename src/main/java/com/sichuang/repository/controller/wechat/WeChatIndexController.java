package com.sichuang.repository.controller.wechat;

import com.sichuang.repository.api.Ewaytec2001API;
import com.sichuang.repository.controller.BaseController;
import com.sichuang.repository.service.IUserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author xs
 * @date 2018/8/1 11:39
 */
@Controller
@RequestMapping("/weChat")
public class WeChatIndexController extends BaseController {

    @Resource
    private IUserService userService;

    @RequestMapping("/message")
    public String wechatMessage(Model model, HttpServletRequest request, String msg) {
        model.addAttribute("msg", msg);
        return "wechat/message";
    }
}
