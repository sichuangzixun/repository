<!DOCTYPE html>
<html>
<head>
    <base href="${basePath}">
    <meta charset="utf-8">
    <title>知识库管理后台系统</title>
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="hplus/css/animate.min.css" rel="stylesheet">
    <link href="hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
    <link href="hplus/css/plugins/jsTree/style.min.css" rel="stylesheet">
</head>
<body class="gray-bg top-navigation" id="page">

<div id="wrapper">

    <div id="page-wrapper" class="gray-bg">

        <div class="row border-bottom white-bg">
            <div class="container">
                <nav class="navbar navbar-static-top" role="navigation">
                    <div class="navbar-header">
                        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar"
                                data-toggle="collapse"
                                class="navbar-toggle collapsed" type="button">
                            <i class="fa fa-reorder"></i>
                        </button>
                    </div>

                    <div class="navbar-collapse collapse" id="navbar">
                        <ul class="nav navbar-nav">
                            <li class="active">
                                <a aria-expanded="false" role="button" href="web/article/"> 首页</a>
                            </li>
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="web/article/list"> 管理文章 </a>
                            </li>
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="web/category/list"> 管理分类 </a>
                            </li>
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="web/suggest/"> 反馈建议 </a>
                            </li>
                        </ul>
                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <form id="searchForm" role="form" class="form-inline m-sm" action="web/article/"
                                      method="post">
                                    <div class="input-group">
                                        <input type="text" placeholder="搜索标题、正文" name="articleContent"
                                               class="form-control" value="${article.articleContent!}">
                                        <input type="hidden" name="topCategoryId" value="${article.topCategoryId!}">
                                        <input type="hidden" name="begin" id="begin" value="${page.begin}"/>
                                        <input type="hidden" name="length" id="length" value="${page.length}"/>
                                        <span class="input-group-btn">
                                            <button type="submit" id="search" class="btn btn-primary">搜索</button>
                                        </span>
                                    </div>
                                </form>
                            </li>
                            <li>
                                <a href="javascript:void(0);" onclick="openWindow('添加文章', 'web/article/add');">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                                   data-toggle="dropdown"> <i class="fa fa-user"></i> </a>
                                <ul role="menu" class="dropdown-menu">
                                    <li><a href="web/user/changePassword">修改密码</a>
                                    </li>
                                    <li><a href="web/user/logout">退出登录</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <div class="container min-height">
                <ol class="breadcrumb">
                    <li><a href="web/article/list"> 管理文章</a></li>
                    <li>
                        <strong>按分类查看文章</strong>
                    </li>
                </ol>
                <div class="row">
                    <div class="col-sm-3">
                        <div id="using_json" style="margin-top:1em; min-height:200px;"></div>
                    </div>
                    <div class="col-sm-9">
                        <span class="pull-left text-muted m">显示 <#if page.end gt 0>${page.begin + 1}<#else>${page.begin}</#if> - ${page.end} 条记录 ，总共 ${page.totalRecords} 条记录</span>
                        <div class="mail-tools tooltip-demo m-sm">
                            <div class="btn-group pull-right">
                                <button id="btn_last" class="btn btn-white btn-sm">
                                    <i class="fa fa-arrow-left"></i>
                                </button>
                                <button id="btn_next" class="btn btn-white btn-sm">
                                    <i class="fa fa-arrow-right"></i>
                                </button>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="client-detail">
                            <div class="full-height-scroll">
                                <div class="feed-activity-list " id="article_list">
                            <#list data as articleItem>
                                <div class="feed-element">
                                    <a href="web/article/view?articleId=${articleItem.articleId}&articleContent=${article.articleContent!}"
                                       class="btn-link text-success">
                                        <h3>
                                            ${articleItem.articleTitle}
                                        </h3>
                                    </a>
                                    <div class="small m-b-xs">
                                        <strong>${articleItem.categoryName!}</strong>
                                        <span class="text-muted">
                                        <i class="fa fa-clock-o"></i> ${articleItem.addTime?datetime}
                                        </span>
                                    </div>
                                    <p class="article_content">${articleItem.articleAbstract!}</p>
                                </div>
                            </#list>
                            <#if data?size == 0>
                                <p class="text-danger">很抱歉，找不到相关文章！</p>
                            </#if>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="pull-right">
                &copy; 2018
                <a href="#" target="_blank">思创智汇（广州）科技有限公司</a>
            </div>
        </div>
    </div>
</div>
<div id="small-chat">
    <a class="open-small-chat" onclick="page_top()">
        <i class="glyphicon glyphicon-menu-up"></i>
    </a>
</div>
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="hplus/js/bootstrap.min.js?v=3.3.6"></script>
<script src="hplus/js/content.min.js?v=1.0.0"></script>
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script src="${basePath + urls.getForLookupPath('/js/function.js')}"></script>
<script src="hplus/js/plugins/jsTree/jstree.min.js"></script>
<script src="plugins/highlight/jquery.highlight.js"></script>
<script src="hplus/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<style>
    /*分类目录*/
    #using_json {
        height: auto;
        max-width: 100%;
        overflow: auto;
        font: 12px Verdana, sans-serif;
        /*padding: 20px;*/
        border-right: 1px solid #cecece;
    }


</style>
<script>

    var successCode = "${successCode}";
    var basePath = "${basePath}";

    $(function () {
        $(".full-height-scroll").slimScroll({height: "100%"});

        $('#using_json').jstree({
            "core": {
                "animation": 0,
                "check_callback": true,//true可修改树结构
                'force_text': true,//escape HTML
                'multiple': false,
                'data': {
                    'url': 'api/category/tree'
                }
            },
            "types": {
                "default": {
                    "valid_children": ["default", "file"]
                },
                "file": {
                    "icon": "glyphicon glyphicon-file",
                    "valid_children": []
                }
            },
            "conditionalselect": function (node, event) {

                if (node.parent == '#') {
                    // console.log(node.parent);
                    $('input[name="topCategoryId"]').val(node.id);
                    window.location.href = "web/article?topCategoryId=" + node.id;
                } else {
                    window.location.href = "web/article?categoryId=" + node.id;
                }

            },
            "plugins": [
                "types", "wholerow", "conditionalselect"/*"contextmenu",*/
            ]
        });

        $("#using_json").on("loaded.jstree", function (event, data) {
            // 展开所有节点
            $('#using_json').jstree('open_all');
            // 选中指定节点
            $('#using_json').jstree('select_node', '${article.categoryId!}');
            $('#using_json').jstree('select_node', '${article.topCategoryId!}');
        });
        //搜索关键字标红
        <#if article.articleContent??  >
            $('#article_list').highlight('${article.articleContent!}');
        </#if>

        var totalRecords = '${page.totalRecords}';

        $('#btn_next').click(function () {
            var length = parseInt($("#length").val());
            var begin = parseInt($("#begin").val()) + length;
            if (begin >= totalRecords) {
                alert("当前页面为最后一页！");
                return false;
            }
            $("#begin").val(begin);
            $("#searchForm").submit();
        });

        $('#btn_last').click(function () {
            var begin = parseInt($("#begin").val());
            var length = parseInt($("#length").val());
            if (begin == 0) {
                alert("当前页面为第一页！");
                return false;
            }
            $("#begin").val(begin - length);
            $("#searchForm").submit();
        });

        $('#search').click(function () {
            $("#begin").val(0);
        });
    });


</script>
</body>
</html>
