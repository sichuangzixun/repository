package com.sichuang.repository.dao;

import com.sichuang.repository.model.entity.Suggest;
import com.sichuang.repository.model.entity.SuggestExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SuggestMapper {
    Integer countByExample(SuggestExample example);

    int deleteByExample(SuggestExample example);

    int deleteByPrimaryKey(Integer suggestId);

    int insert(Suggest record);

    int insertSelective(Suggest record);

    List<Suggest> selectByExample(SuggestExample example);

    Suggest selectByPrimaryKey(Integer suggestId);

    int updateByExampleSelective(@Param("record") Suggest record, @Param("example") SuggestExample example);

    int updateByExample(@Param("record") Suggest record, @Param("example") SuggestExample example);

    int updateByPrimaryKeySelective(Suggest record);

    int updateByPrimaryKey(Suggest record);
}