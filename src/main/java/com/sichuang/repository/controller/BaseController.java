package com.sichuang.repository.controller;


import com.alibaba.fastjson.JSON;
import com.sichuang.repository.common.MyResponse;
import com.sichuang.repository.config.BaseConfig;
import com.sichuang.repository.model.entity.User;
import com.sichuang.repository.other.EnumManage;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;


public class BaseController extends MyResponse {
    private static HttpSession httpSession;
    public static final String TimeFormat = "yyyy-MM-dd HH:mm:ss";
    public static final String DateFormat = "yyyy-MM-dd";

    //上传图片大小限制
    public static final Long MaxUpLoadImageSize = (long) (150 * 1024);
    // 路径配置
    public static final String URL = "/repository/";
    public static final String LoginUrl = "/repository/login?";
    public static final String AdminMsgUrl = "/repository/message?";
    public static final String MobileMsgUrl = "/repository/weChat/message?";

    /**
     * 根据session 判断用户是否登录
     *
     * @param httpServletRequest
     * @return
     */
    protected boolean isLogin(HttpServletRequest httpServletRequest) {
        User userSession = getUserSession(httpServletRequest);
        if (userSession != null) {
            return true;
        }
        return false;
    }


    /**
     * 保存用户session信息
     *
     * @param request
     * @param userInfo
     */
    public static void saveUserSession(HttpServletRequest request, User userInfo) {
        userInfo.setPassword(null);
        httpSession = request.getSession();
        if (userInfo.getUserType().equals(EnumManage.UserTypeEnum.Normal.getId())) {
            //session过期时间1个小时
            httpSession.setMaxInactiveInterval(60 * 60);
        }

        httpSession.setAttribute("userSession", userInfo);
    }

    /**
     * 获取session保存用户信息
     *
     * @param request
     * @return
     */
    public static User getUserSession(HttpServletRequest request) {
        httpSession = request.getSession();
        User userSession = (User) httpSession.getAttribute("userSession");
        return userSession;
    }

    /**
     * 获取当前服务器路径
     *
     * @param request
     * @return
     */
    public static String getBasePath(HttpServletRequest request) {//+ request.getServerName() + ":" + request.getServerPort()
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://" + BaseConfig.domainName + path
                + "/";
        return basePath;
    }

    /**
     * 判断文件名是否为GIF、PNG、JPG类型
     *
     * @param fileName
     * @return
     */
    public static boolean isImageType(String fileName) {
        String type = null;// 文件类型
        //System.out.println("上传的文件原名称:"+fileName);
        // 判断文件类型
        type = fileName.indexOf(".") != -1 ? fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length()) : null;
        if (type != null) {// 判断文件类型是否为空
            if ("GIF".equals(type.toUpperCase()) || "PNG".equals(type.toUpperCase()) || "JPG".equals(type.toUpperCase())) {
                return true;
            }
        }
        return false;
    }

    /**
     * 上传图片大小、格式是否符合要求
     *
     * @param image
     * @return
     */
    public static Map<String, Object> isUploadImage(MultipartFile image) {
        String msg = "操作失败！";
        if (image != null && image.isEmpty() == false) {
            //判断文件大小
            if (image.getSize() < BaseController.MaxUpLoadImageSize) {
                if (isImageType(image.getOriginalFilename())) {
                    return MyResponse.getDefaultSuccessResponse();
                } else {
                    return MyResponse.getErrorResponse(msg + "文件格式不是图片类型！");
                }
            } else {
                return MyResponse.getErrorResponse(msg + "文件大小超过限制！");
            }
        }
        return MyResponse.getErrorResponse(msg + "文件不能为空！");
    }

    /**
     * 返回suggest搜索数据
     *
     * @param callBack
     * @param data
     * @return
     */
    public static String getSuggestCallBack(String callBack, Object data) {
        return callBack + "(" + JSON.toJSONString(data) + ")";
    }

}
