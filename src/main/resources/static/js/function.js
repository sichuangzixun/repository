/**
 * Created by sc on 2017-3-10.
 */
/**
 * 返回页面顶部
 */
function page_top() {
    $("html,body").animate({scrollTop: 0}, 500);
}

/**
 * ajax请求失败
 */
$(document).ajaxError(function () {
    if(layer){
        layer.closeAll('loading');
    }
    alert("服务器请求失败！");
});

/**
 * ajax返回结果处理。
 * @param data
 * @param basePath
 */
function responseData(data, basePath) {
    if(layer){
        layer.closeAll('loading');
    }
    if (data.hasOwnProperty("code") == false) {
        alert("请求失败！");
    }
    if (data.code == -1) {
        top.location.href = basePath;
    }
    if (data.code == 40001) {
        alert(data.msg);
    }
}

/**
 * 分页信息
 * @param res
 * @param basePath
 * @returns {{data: Array, total: number}}
 */
function responsePageData(res, basePath) {
    responseData(res, basePath);
    var data = [];
    var total = 0;
    if (res.code == successCode) {
        data = res.data;
        total = res.page.totalRecords;
    }
    return {
        "data": data,
        "total": total,
    };
}

/**
 * 初始化select选中选项
 * @param select
 * @param values
 */
function chose_mult_set_ini(select, values) {
    var arr = values.split(',');
    var length = arr.length;
    var value = '';
    for (var i = 0; i < length; i++) {
        value = arr[i];
        if (value != "") {
            $(select + " option[value='" + value + "']").attr('selected',
                'selected');
        }

    }
}

/**
 * 验证数组是否有重复数据
 * @param arr
 * @returns {boolean}
 */
function arrRepeat(arr) {
    var arrStr = JSON.stringify(arr), str;
    for (var i = 0; i < arr.length; i++) {
        if (arrStr.replace(arr[i] + ",", "").indexOf(arr[i] + ",") > -1) {
            alert(arr[i]);
            return true;
        }
    }
    return false;
}

/**
 * 打开iframe窗。layer插件
 * @param title
 * @param url
 */
function openWindow(title, url, refresh) {
    layer.closeAll();
    layer.open({
        id: 'editWindow',
        type: 2,
        title: title,
        shadeClose: true,
        // shade: true,
        area: ['100%', '100%'],
        content: url
        , end: function () {
            refresh = refresh == null ? true : false;
            if (refresh) {
                //右上角关闭回调
                window.location.reload();
            }
        }
    });
}

/**
 * 格式化日期时间
 * @param data
 * @returns {string|*}
 */
function dateTimeFormatter(data) {
    return DateFormat(data, 'yyyy-MM-dd hh:mm')
}

/**
 * 格式化日期
 * @param data
 * @returns {string|*}
 */
function dateFormatter(data) {
    return DateFormat(data, 'yyyy-MM-dd')
}

/**
 * 格式化时间
 * @param data
 * @returns {string|*}
 */
function timeFormatter(data) {
    return DateFormat(data, 'hh:mm')
}

/**
 * 时间戳格式化
 * @param timestamp
 * @param format
 * @returns
 */
function DateFormat(timestamp, format) {
    if (timestamp == null) {
        return "-";
    }
    timestamp = new Date(timestamp);
    var date = {
        "M+": timestamp.getMonth() + 1,
        "d+": timestamp.getDate(),
        "h+": timestamp.getHours(),
        "m+": timestamp.getMinutes(),
        "s+": timestamp.getSeconds(),
        "q+": Math.floor((timestamp.getMonth() + 3) / 3),
        "S+": timestamp.getMilliseconds()
    };
    if (/(y+)/i.test(format)) {
        format = format.replace(RegExp.$1, (timestamp.getFullYear() + '')
            .substr(4 - RegExp.$1.length));
    }
    for (var k in date) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? date[k]
                : ("00" + date[k]).substr(("" + date[k]).length));
        }
    }
    return format;
}


/**
 * 获取文章显示内容
 * @param article
 * @returns {string}
 */
function getArticleHtml(basePath, article, articleContent) {
    var text = '<li data-icon="false" >\n' +
        '                <a href="' + basePath + 'weChat/article/info?articleId=' + article.articleId + '&articleContent=' + articleContent+'" data-ajax="false">\n' +
        '                    <h2>' + article.articleTitle + '</h2>\n' +
        '                    <p><span class="text-primary">' + article.categoryName + '</span>\n' +
        '                        ' + article.updateTime + '</p>\n' +
        '                </a>\n' +
        '            </li>';

    return text;
}