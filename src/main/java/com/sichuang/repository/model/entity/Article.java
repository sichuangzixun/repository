package com.sichuang.repository.model.entity;

import java.io.Serializable;
import java.util.Date;

public class Article implements Serializable {
    /** 文章id  article_id **/
    private Integer articleId;

    /** 文章标题。不能为空。  article_title **/
    private String articleTitle;

    /** 一级分类id  top_category_id **/
    private Integer topCategoryId;

    /** 分类id。不能为空。  category_id **/
    private Integer categoryId;

    /** 分类名称。不能为空。  category_name **/
    private String categoryName;

    /** 文章作者。不能为空。  article_author **/
    private String articleAuthor;

    /** 创建时间。不能为空。  add_time **/
    private Date addTime;

    /** 删除状态。不能为空。默认未删除0.  is_delete **/
    private Boolean isDelete;

    /** 文章摘要  article_abstract **/
    private String articleAbstract;

    /** 是否置顶  is_top **/
    private Boolean isTop;

    /** 修改时间  update_time **/
    private Date updateTime;

    /** 文章内容。不能为空。  article_content **/
    private String articleContent;

    /**   tableName: article   **/
    private static final long serialVersionUID = 1L;

    /**   文章id  article_id   **/
    public Integer getArticleId() {
        return articleId;
    }

    /**   文章id  article_id   **/
    public void setArticleId(Integer articleId) {
        this.articleId = articleId;
    }

    /**   文章标题。不能为空。  article_title   **/
    public String getArticleTitle() {
        return articleTitle;
    }

    /**   文章标题。不能为空。  article_title   **/
    public void setArticleTitle(String articleTitle) {
        this.articleTitle = articleTitle == null ? null : articleTitle.trim();
    }

    /**   一级分类id  top_category_id   **/
    public Integer getTopCategoryId() {
        return topCategoryId;
    }

    /**   一级分类id  top_category_id   **/
    public void setTopCategoryId(Integer topCategoryId) {
        this.topCategoryId = topCategoryId;
    }

    /**   分类id。不能为空。  category_id   **/
    public Integer getCategoryId() {
        return categoryId;
    }

    /**   分类id。不能为空。  category_id   **/
    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    /**   分类名称。不能为空。  category_name   **/
    public String getCategoryName() {
        return categoryName;
    }

    /**   分类名称。不能为空。  category_name   **/
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName == null ? null : categoryName.trim();
    }

    /**   文章作者。不能为空。  article_author   **/
    public String getArticleAuthor() {
        return articleAuthor;
    }

    /**   文章作者。不能为空。  article_author   **/
    public void setArticleAuthor(String articleAuthor) {
        this.articleAuthor = articleAuthor == null ? null : articleAuthor.trim();
    }

    /**   创建时间。不能为空。  add_time   **/
    public Date getAddTime() {
        return addTime;
    }

    /**   创建时间。不能为空。  add_time   **/
    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    /**   删除状态。不能为空。默认未删除0.  is_delete   **/
    public Boolean getIsDelete() {
        return isDelete;
    }

    /**   删除状态。不能为空。默认未删除0.  is_delete   **/
    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    /**   文章摘要  article_abstract   **/
    public String getArticleAbstract() {
        return articleAbstract;
    }

    /**   文章摘要  article_abstract   **/
    public void setArticleAbstract(String articleAbstract) {
        this.articleAbstract = articleAbstract == null ? null : articleAbstract.trim();
    }

    /**   是否置顶  is_top   **/
    public Boolean getIsTop() {
        return isTop;
    }

    /**   是否置顶  is_top   **/
    public void setIsTop(Boolean isTop) {
        this.isTop = isTop;
    }

    /**   修改时间  update_time   **/
    public Date getUpdateTime() {
        return updateTime;
    }

    /**   修改时间  update_time   **/
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**   文章内容。不能为空。  article_content   **/
    public String getArticleContent() {
        return articleContent;
    }

    /**   文章内容。不能为空。  article_content   **/
    public void setArticleContent(String articleContent) {
        this.articleContent = articleContent == null ? null : articleContent.trim();
    }
}