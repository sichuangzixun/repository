package com.sichuang.repository.model.entity;

import java.io.Serializable;
import java.util.Date;

public class Suggest implements Serializable {
    /** 建议id  suggest_id **/
    private Integer suggestId;

    /** 建议内容  suggest_content **/
    private String suggestContent;

    /** 添加时间  add_time **/
    private Date addTime;

    /** 添加用户姓名  add_user_name **/
    private String addUserName;

    /** 用户id  add_user_id **/
    private Integer addUserId;

    /** 回复内容  reply_message **/
    private String replyMessage;

    /** 回复时间  reply_time **/
    private Date replyTime;

    /** 回复用户  reply_user_name **/
    private String replyUserName;

    /** 删除状态  is_delete **/
    private Boolean isDelete;

    /** 处理状态：0-未处理，1-已处理  is_processed **/
    private Integer isProcessed;

    /**   tableName: suggest   **/
    private static final long serialVersionUID = 1L;

    /**   建议id  suggest_id   **/
    public Integer getSuggestId() {
        return suggestId;
    }

    /**   建议id  suggest_id   **/
    public void setSuggestId(Integer suggestId) {
        this.suggestId = suggestId;
    }

    /**   建议内容  suggest_content   **/
    public String getSuggestContent() {
        return suggestContent;
    }

    /**   建议内容  suggest_content   **/
    public void setSuggestContent(String suggestContent) {
        this.suggestContent = suggestContent == null ? null : suggestContent.trim();
    }

    /**   添加时间  add_time   **/
    public Date getAddTime() {
        return addTime;
    }

    /**   添加时间  add_time   **/
    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    /**   添加用户姓名  add_user_name   **/
    public String getAddUserName() {
        return addUserName;
    }

    /**   添加用户姓名  add_user_name   **/
    public void setAddUserName(String addUserName) {
        this.addUserName = addUserName == null ? null : addUserName.trim();
    }

    /**   用户id  add_user_id   **/
    public Integer getAddUserId() {
        return addUserId;
    }

    /**   用户id  add_user_id   **/
    public void setAddUserId(Integer addUserId) {
        this.addUserId = addUserId;
    }

    /**   回复内容  reply_message   **/
    public String getReplyMessage() {
        return replyMessage;
    }

    /**   回复内容  reply_message   **/
    public void setReplyMessage(String replyMessage) {
        this.replyMessage = replyMessage == null ? null : replyMessage.trim();
    }

    /**   回复时间  reply_time   **/
    public Date getReplyTime() {
        return replyTime;
    }

    /**   回复时间  reply_time   **/
    public void setReplyTime(Date replyTime) {
        this.replyTime = replyTime;
    }

    /**   回复用户  reply_user_name   **/
    public String getReplyUserName() {
        return replyUserName;
    }

    /**   回复用户  reply_user_name   **/
    public void setReplyUserName(String replyUserName) {
        this.replyUserName = replyUserName == null ? null : replyUserName.trim();
    }

    /**   删除状态  is_delete   **/
    public Boolean getIsDelete() {
        return isDelete;
    }

    /**   删除状态  is_delete   **/
    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    /**   处理状态：0-未处理，1-已处理  is_processed   **/
    public Integer getIsProcessed() {
        return isProcessed;
    }

    /**   处理状态：0-未处理，1-已处理  is_processed   **/
    public void setIsProcessed(Integer isProcessed) {
        this.isProcessed = isProcessed;
    }
}