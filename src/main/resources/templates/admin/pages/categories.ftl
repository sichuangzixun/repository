<!DOCTYPE html>
<html>
<head>
    <base href="${basePath}">
    <meta charset="utf-8">
    <title>知识库管理后台系统</title>
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="hplus/css/animate.min.css" rel="stylesheet">
    <link href="hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
    <link href="hplus/css/plugins/jsTree/style.min.css" rel="stylesheet">
</head>
<body class="gray-bg top-navigation" id="page">

<div id="wrapper">

    <div id="page-wrapper" class="gray-bg">

        <div class="row border-bottom white-bg">
            <div class="container">
                <nav class="navbar navbar-static-top" role="navigation">
                    <div class="navbar-header">
                        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar"
                                data-toggle="collapse"
                                class="navbar-toggle collapsed" type="button">
                            <i class="fa fa-reorder"></i>
                        </button>
                    </div>

                    <div class="navbar-collapse collapse" id="navbar">
                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="web/article/"> 首页</a>
                            </li>
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="web/article/list"> 管理文章 </a>
                            </li>
                            <li class="active">
                                <a aria-expanded="false" role="button" href="web/category/list"> 管理分类 </a>
                            </li>
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="web/suggest/"> 反馈建议 </a>
                            </li>
                        </ul>
                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <form role="form" class="form-inline m-sm" action="web/article/" method="post">
                                    <div class="input-group">
                                        <input type="text" placeholder="搜索标题、正文" name="articleContent"
                                               class="form-control">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-primary">搜索</button>
                                        </span>
                                    </div>
                                </form>
                            </li>
                            <li>
                                <a href="javascript:void(0);" onclick="openWindow('添加文章', 'web/article/add');">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                                   data-toggle="dropdown"> <i class="fa fa-user"></i> </a>
                                <ul role="menu" class="dropdown-menu">
                                    <li><a href="web/user/changePassword">修改密码</a>
                                    </li>
                                    <li><a href="web/user/logout">退出登录</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <div class="container min-height">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="web/article"> 首页</a></li>
                            <li>
                                <strong>管理分类</strong>
                            </li>
                        </ol>
                        <div class="alert alert-warning alert-dismissable">
                            1、选中分类后点+ 可以增加子分类，可输入分类名；选择分类后可以点击编辑分类名称，或点击删除此分类；可拖动分类排序。
                            <br>2、最多支持3级分类；父分类删除后，其下的子分类也全部删除。该分类及下级分类的文章变为其他分类。一级分类不能删除。
                        </div>
                        <div class="tooltip-demo">
                            <button type="button" class="btn btn-primary btn-sm" onclick="add();">
                                <i class="glyphicon glyphicon-plus"></i> 添加
                            </button>
                            <button type="button" class="btn btn-success btn-sm" onclick="update();">
                                <i class="glyphicon glyphicon-pencil"></i> 编辑
                            </button>
                            <button type="button" class="btn btn-danger btn-sm" onclick="remove();">
                                <i class="glyphicon glyphicon-remove"></i> 删除
                            </button>
                        </div>
                        <div id="using_json" style="margin-top:1em; min-height:200px;max-height: 600px;"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="pull-right">
                &copy; 2018
                <a href="#" target="_blank">思创智汇（广州）科技有限公司</a>
            </div>
        </div>
    </div>
</div>
<div id="small-chat">
    <a class="open-small-chat" onclick="page_top()">
        <i class="glyphicon glyphicon-menu-up"></i>
    </a>
</div>
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="hplus/js/bootstrap.min.js?v=3.3.6"></script>
<script src="hplus/js/content.min.js?v=1.0.0"></script>
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script src="${basePath + urls.getForLookupPath('/js/function.js')}"></script>
<script src="hplus/js/plugins/jsTree/jstree.min.js"></script>
<style>
    #using_json {
        max-width: 100%;
        overflow: auto;
        font: 10px Verdana, sans-serif;
        box-shadow: 0 0 5px #ccc;
        padding: 10px;
        border-radius: 5px;
    }
</style>
<script>

    var successCode = "${successCode}";
    var basePath = "${basePath}";


    function add() {
        var ref = $('#using_json').jstree(true);
        var sel = ref.get_selected(true);
        if (!sel.length) {
            alert('不能添加一级分类！');
            return false;
            // sel = '#';
        } else {
            sel = sel[0];
        }
        if (sel.parents.length > 2) {
            alert('最多支持3级分类！');
            return false;
        }
        sel = ref.create_node(sel, {"type": "default"});
        if (sel) {
            ref.edit(sel, '', function (node, status, cancel) {
                if ($.trim(node.text) == '') {
                    // alert('分类名称不能为空！');
                    ref.delete_node(node);
                    return false;
                }
                if (cancel == false && status) {
                    var url = 'api/category/add';
                    var parentId = node.parent == '#' ? 0 : node.parent;
                    var vPost = "categoryName=" + node.text + "&parentId=" + parentId;
                    $.post(url, vPost, function (data) {
                        responseData(data, basePath);
                        if (data.code == successCode) {
                            ref.set_id(node, data.data.categoryId);
                        }
                    });
                }
            });
        }
    }

    function update() {
        var ref = $('#using_json').jstree(true),
                sel = ref.get_selected(true);

        if (!sel.length) {
            alert('请选择分类！');
            return false;
        }
        sel = sel[0];
        if (ref.get_node(sel).parent == '#') {
            alert("根节点不允许修改");
            return;
        }
        var text = sel.text;
        ref.edit(sel, null, function (node, status, cancel) {
            if (node.text != text) {
                var url = "api/category/update?categoryId=" + node.id;
                var parentId = node.parent == '#' ? 0 : node.parent;
                var vPost = "categoryName=" + node.text + "&parentId=" + parentId;
                $.post(url, vPost, function (data) {
                    responseData(data, basePath);
                });
            }
        });
    }

    function remove() {
        if (!confirm('确认要删除选定的项目吗？'))
            return;
        var ref = $('#using_json').jstree(true);
        var sel = ref.get_selected();
        if (!sel.length) {
            alert('请选择分类！');
            return false;
        }
        sel = sel[0];
        if (ref.get_node(sel).parent == '#') {
            alert("根节点不允许删除");
            return;
        }
        remove_category(sel);
    }

    //操作
    function remove_category(categoryId) {
        var ref = $('#using_json').jstree(true);
        var url = 'api/category/remove';
        var vPost = "categoryId=" + categoryId;
        $.post(url, vPost, function (data) {
            responseData(data, basePath);
            if (data.code == successCode) {
                alert(data.msg);
                ref.delete_node(categoryId);
            }
        });
    }


    $(function () {
        $('#using_json').on('move_node.jstree', function (e, data) {
            var node = data.node;
            var categoryOrder = data.position + 1;
            var parentId = data.parent == '#' ? 0 : data.parent;
            var url = "api/category/update?categoryId=" + node.id;
            var vPost = "categoryOrder=" + categoryOrder + "&parentId=" + parentId;
            $.post(url, vPost, function (data) {
                responseData(data, basePath);
            });

        }).jstree({
            "core": {
                "animation": 0,
                "check_callback": true,//true可修改树结构
                'force_text': true,//escape HTML
                'multiple': false,
                'data': {
                    'url': 'api/category/tree'
                }
            },
            "types": {
                "default": {
                    "valid_children": ["default", "file"]
                },
                "file": {
                    "icon": "glyphicon glyphicon-file",
                    "valid_children": []
                }
            },
            "plugins": [
                "dnd", "search", "types", "wholerow"/*"contextmenu",*/
            ]
        });
        $("#using_json").on("loaded.jstree", function (event, data) {
            // 展开所有节点
            $('#using_json').jstree('open_all');
        });

    });


</script>
</body>
</html>
