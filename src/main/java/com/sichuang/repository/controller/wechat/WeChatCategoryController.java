package com.sichuang.repository.controller.wechat;

import com.alibaba.fastjson.JSON;
import com.sichuang.repository.service.ICategoryService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;


/**
 * @author xs
 * @date 2018/8/29 14:40
 */
@Controller
@RequestMapping("/weChat/category")
public class WeChatCategoryController {

    @Resource
    private ICategoryService categoryService;

    @RequestMapping("")
    public String index(Model model, Integer categoryId){
        if(categoryId == null || categoryId <= 0){
            categoryId = 1;
        }
        model.addAttribute("treeNodeList", JSON.toJSONString(categoryService.getChildrenByParentId(categoryId, true)));
        model.addAttribute("categoryId", categoryId);
        return "wechat/categoryTree";
    }
}
