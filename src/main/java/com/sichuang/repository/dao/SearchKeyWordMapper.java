package com.sichuang.repository.dao;

import com.sichuang.repository.model.entity.SearchKeyWord;
import com.sichuang.repository.model.entity.SearchKeyWordExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SearchKeyWordMapper {
    Integer countByExample(SearchKeyWordExample example);

    int deleteByExample(SearchKeyWordExample example);

    int deleteByPrimaryKey(String keyWord);

    int insert(SearchKeyWord record);

    int insertSelective(SearchKeyWord record);

    List<SearchKeyWord> selectByExample(SearchKeyWordExample example);

    SearchKeyWord selectByPrimaryKey(String keyWord);

    int updateByExampleSelective(@Param("record") SearchKeyWord record, @Param("example") SearchKeyWordExample example);

    int updateByExample(@Param("record") SearchKeyWord record, @Param("example") SearchKeyWordExample example);

    int updateByPrimaryKeySelective(SearchKeyWord record);

    int updateByPrimaryKey(SearchKeyWord record);
}