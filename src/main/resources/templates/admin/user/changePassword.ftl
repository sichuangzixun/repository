<!DOCTYPE html>
<html>
<head>
    <base href="${basePath}">
    <meta charset="utf-8">
    <title>知识库管理后台系统</title>
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="hplus/css/animate.min.css" rel="stylesheet">
    <link href="hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
</head>
<body class="gray-bg top-navigation" id="page">

<div id="wrapper">

    <div id="page-wrapper" class="gray-bg">

        <div class="row border-bottom white-bg">
            <div class="container">
                <nav class="navbar navbar-static-top" role="navigation">
                    <div class="navbar-header">
                        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar"
                                data-toggle="collapse"
                                class="navbar-toggle collapsed" type="button">
                            <i class="fa fa-reorder"></i>
                        </button>
                    </div>

                    <div class="navbar-collapse collapse" id="navbar">
                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="web/article/"> 首页</a>
                            </li>
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="web/article/list"> 管理文章 </a>
                            </li>
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="web/category/list"> 管理分类 </a>
                            </li>
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="web/suggest/"> 反馈建议 </a>
                            </li>
                        </ul>
                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <form role="form" class="form-inline m-sm" action="web/article/" method="post">
                                    <div class="input-group">
                                        <input type="text" placeholder="搜索标题、正文" name="articleContent" class="form-control">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-primary">搜索</button>
                                        </span>
                                    </div>
                                </form>
                            </li>
                            <li>
                                <a href="javascript:void(0);" onclick="openWindow('添加文章', 'web/article/add');" >
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                                   data-toggle="dropdown"> <i class="fa fa-user"></i> </a>
                                <ul role="menu" class="dropdown-menu">
                                    <li><a href="web/user/changePassword">修改密码</a>
                                    </li>
                                    <li><a href="web/user/logout">退出登录</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <div class="container min-height">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li>
                                <strong>修改密码</strong>
                            </li>
                        </ol>
                        <form class="form-horizontal m-t" id="userForm">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">用户名：</label>
                                <div class="col-sm-9">
                                    <input name="loginName" type="text" readonly="readonly"
                                           value="${loginName }"
                                           class="form-control" placeholder=""/>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">旧密码：</label>
                                <div class="col-sm-9">
                                    <input type="password" id="oldPwd" class="form-control" placeholder="">
                                    <input type="hidden" name="oldPassword"/>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">新密码：</label>
                                <div class="col-sm-9">
                                    <input type="password" id="pwd" class="form-control" placeholder="">
                                    <input type="hidden" name="newPassword"/>
                                    <span class="help-block m-b-none">
										<i class="fa fa-info-circle"></i>
										密码长度为5-20个字符
									</span>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">确认新密码：</label>
                                <div class="col-sm-9">
                                    <input type="password" id="pwd2" class="form-control" placeholder="">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="control-label"></label>
                                <div class="col-sm-9 text-left">
                                    <button class="btn btn-primary" type="button" id="submit">提交</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="pull-right">
                &copy; 2018
                <a href="#" target="_blank">思创智汇（广州）科技有限公司</a>
            </div>
        </div>
    </div>
</div>
<div id="small-chat">
    <a class="open-small-chat" onclick="page_top()">
        <i class="glyphicon glyphicon-menu-up"></i>
    </a>
</div>
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="hplus/js/bootstrap.min.js?v=3.3.6"></script>
<script src="hplus/js/content.min.js?v=1.0.0"></script>
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script src="${basePath + urls.getForLookupPath('/js/function.js')}"></script>
<script src="js/md5.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        var needLoginCode = "${needLoginCode}";
        var basePath = "${basePath}";
        var successCode = "${successCode}";

        $('#submit').click(function () {

            var vPwd = $('#pwd').val();
            var vPwd2 = $('#pwd2').val();
            var oldPwd = $('#oldPwd').val();
            if (oldPwd == '') {
                alert("请输入旧密码！");
                return false;
            }
            if (vPwd == '') {
                alert("请输入新密码！");
                return false;
            }
            if (vPwd2 == '') {
                alert("请再次输入新密码！");
                return false;
            }
            if (vPwd == oldPwd) {
                alert("新密码与旧密码相同，请重新输入！");
                return false;
            }
            if (vPwd != vPwd2) {
                alert("两次输入新密码不同，请重新输入新密码！");
                return false;
            }
            if (vPwd.length < 5 || vPwd.length > 20) {
                alert("密码长度为5-20个字符！");
                return false;
            }

            $('#userForm input[name="newPassword"]').val(hex_md5(vPwd));

            $('#userForm input[name="oldPassword"]').val(hex_md5(oldPwd));
            var url = "api/user/changePassword";
            var vPost = $('#userForm').serialize();
            $.post(url, vPost, function (data) {
                responseData(data, basePath);
                if (data.code == successCode) {
                    alert(data.msg);
                    top.location.href = basePath + 'web/user/logout';
                }
            });
        });

    });

</script>
<style>
    form input, select {
        min-width: 200px;
        max-width: 350px;
    }
</style>
</body>
</html>
