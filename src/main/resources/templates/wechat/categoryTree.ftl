<!DOCTYPE html>
<html>
<head>
    <base href="${basePath}">
    <meta charset="utf-8">
    <title>${weChatTitle}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="mobile/css/jquery.mobile-1.4.5.min.css">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link rel="stylesheet" href="${basePath + urls.getForLookupPath('/mobile/css/mobile-style.css')}">
</head>
<body>
<div data-role="page" id="categoryPage">
    <div data-role="main" class="ui-content">
        <div class="ui-grid-a">
            <div class="ui-block-i">
                <div data-role="navbar" class="ui-field-contain">
                    <fieldset data-role="controlgroup" data-type="vertical">
                        <a href="${basePath}weChat/category?categoryId=1" id="category_1" class="category_list" data-ajax="false">
                            <img class="icon" src="mobile/css/images/01.jpg" />系统操作
                        </a>
                        <a href="${basePath}weChat/category?categoryId=2" id="category_2" class="category_list" data-ajax="false">
                            <img class="icon" src="mobile/css/images/02.jpg" />服务场景
                        </a>
                        <a href="${basePath}weChat/category?categoryId=3" id="category_3" class="category_list" data-ajax="false">
                            <img class="icon" src="mobile/css/images/03.jpg" />专题知识
                        </a>
                        <a href="${basePath}weChat/category?categoryId=4" id="category_4" class="category_list" data-ajax="false">
                            <img class="icon" src="mobile/css/images/04.jpg" />常见问题
                        </a>
                        <a href="${basePath}weChat/category?categoryId=5" id="category_5" class="category_list" data-ajax="false">
                            <img class="icon" src="mobile/css/images/05.jpg" />其他
                        </a>
                    </fieldset>
                </div>
            </div>
            <div class="ui-block-t" id="treeNode">

            </div>
        </div>

    </div>
    <div data-role="footer">
        <div data-role="navbar" data-iconpos="left" class="menu">
            <ul>
                <li><a href="${basePath}weChat/article" data-icon="home" data-ajax="false">主页</a></li>
                <li><a href="${basePath}weChat/category" data-icon="bullets" data-ajax="false" class="active">索引</a></li>
                <li><a href="${basePath}weChat/suggest" data-icon="mail" data-ajax="false">反馈</a></li>
            </ul>
        </div>
    </div>
</div>

<!-- jQuery -->
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="mobile/js/jquery.mobile-1.4.5.min.js"></script>
<style>
    <#--一级分类图标-->
    .category_list {
        background-color: rgba(0, 0, 0, 0.5);
        opacity: 0.6;
        border-radius: 0.5em;
    }

    .active {
        opacity: 5;
    }

    .ui-block-i {
        width: 5em !important;
        float: left;
    }

    .ui-block-t {
        max-width: 100%;
        margin-left: 5em;
        margin-top: -1em;
    }
    ul{
        padding-left: 1em;
    }
    ul li{
        display: block;
    }
    li a{
        text-decoration: none !important;
        text-overflow: ellipsis;
        overflow: hidden;
        font-size: 10.5pt;
    }
</style>
<script>
    $(document).on("pagecreate", "#categoryPage", function () {

        //生成索引
        var treeNodeList = JSON.parse("${treeNodeList?json_string}");
        var text = "";
        for (var i = 0; i < treeNodeList.length; i++) {
            text += getNodeText(treeNodeList[i]);
        }
        if($.trim(text) != ""){
            text = "<ul  data-role='listview'>"+ text+" </ul>";
        }else{
            text = "<ul >该分类暂无文章！</ul>";
        }
        $("#treeNode").append(text);

        //选中一级分类
        $("#category_${categoryId}").addClass("active");

        function getNodeText(treeNode) {
            var node = "";
            var text = getText(treeNode);//获取内容
            if (treeNode.type == "file") {
                return text;
            } else {
                var len = 0;
                if (treeNode.children != null) {
                    len = treeNode.children.length;
                }
                //显示折叠列表
                node = " <li>" + text + "</li>";
                if (len > 0) {
                    node += "<ul data-role='listview'>";
                    for (var i = 0; i < treeNode.children.length; i++) {
                        node += getNodeText(treeNode.children[i]);
                    }
                    node += " </ul>";
                }

                return node;
            }

        }

        function getText(treeNode) {
            //根据类型显示图标
            if (treeNode.type == "file") {
                //文章
                return "<li><a href='${basePath}weChat/article/info?articleId=" + treeNode.id + "' data-ajax='false'><span class='fa fa-file-text-o text-info' ></span> " + treeNode.text + "</a></li>";
            } else {
                //分类
                return "<a href='${basePath}weChat/article/list?categoryId=" + treeNode.id + "' data-ajax='false'><span class='fa fa-folder text-primary'></span> " + treeNode.text + "</a>";

            }
        }

    });
</script>
</body>
</html>
