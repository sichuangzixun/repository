<!DOCTYPE html>
<html>
<head>
    <base href="${basePath}">
    <meta charset="utf-8">
    <title>${weChatTitle}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="mobile/css/jquery.mobile-1.4.5.min.css">
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link rel="stylesheet" href="${basePath + urls.getForLookupPath('/mobile/css/mobile-style.css')}">
</head>
<body>
<div data-role="page">
    <div data-role="main" class="ui-content">
        <form method="post" action="" id="inputForm">
            <h3>反馈建议</h3>
            <label for="suggestContent">反馈建议：</label>
            <textarea name="suggestContent" id="suggestContent" ></textarea>
            <label for="addUserName">反馈人姓名：</label>
            <input type="text" name="addUserName" id="addUserName">
            <input type="button" value="提交" onclick="save()" data-icon="check" data-iconpos="right" data-inline="true">
            <a href="${basePath}weChat/suggest/list" data-ajax="false" class="btn">查看历史记录</a>
        </form>

    </div>
    <div data-role="footer">
        <div data-role="navbar" data-iconpos="left" class="menu">
            <ul>
                <li><a href="${basePath}weChat/article" data-icon="home" data-ajax="false">主页</a></li>
                <li><a href="${basePath}weChat/category" data-icon="bullets" data-ajax="false">索引</a></li>
                <li><a href="${basePath}weChat/suggest" data-icon="mail" data-ajax="false" class="active">反馈</a></li>
            </ul>
        </div>
    </div>
</div>

<!-- jQuery -->
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="mobile/js/jquery.mobile-1.4.5.min.js"></script>
<script src="${basePath + urls.getForLookupPath('/js/function.js')}"></script>
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script>

    var basePath = "${basePath}";
    var successCode = "${successCode}";

    var save = function () {

        if(valid_form()){
            var vPost = $("#inputForm").serialize();
            var url = 'suggest/add';
            $.post(url, vPost, function (data) {
                responseData(data, basePath);
                if (data.code == successCode) {
                    alert(data.msg);
                    top.location.href = basePath+"weChat/suggest/list";
                }
            });
        }

    };

    function valid_form() {
        if($.trim($("textarea[name='suggestContent']").val()) == ''){
            alert("反馈建议不能为空！");
            return false;
        }
        if($.trim($("input[name='addUserName']").val()) == ''){
            alert("反馈人姓名不能为空！");
            return false;
        }

        return true;
    }
</script>
</body>
</html>
