package com.sichuang.repository.service;

import com.alibaba.fastjson.JSONObject;
import com.sichuang.repository.model.entity.Page;
import com.sichuang.repository.model.entity.User;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public interface IUserService {

    /**
     * 登陆
     * @param user
     * @return
     */
    public Map<String, Object> login(HttpServletRequest request, User user) throws Exception;

    /**
     * 用户退出登录
     * @return
     */
    public Map<String, Object> logout(HttpServletRequest request) ;

    /**
     * 修改密码
     * @param oldPassword
     * @param newPassword
     * @return
     */
    public Map<String, Object> changePassword(HttpServletRequest request, String oldPassword, String newPassword) throws Exception;

    /**
     * 获取用户列表
     * @return
     */
    Map<String, Object> search(Page page);

    /**
     * 保存用户
     * @param user
     * @return
     */
    Map<String, Object> add(User user) throws Exception;


    /**
     * 验证用户是否存在
     * @param userId
     * @return
     */
    User verifyUser(Integer userId) throws Exception;

    /**
     * 微信端验证登录
     * @param request
     * @return
     */
    Map<String, Object> weChatLogin(HttpServletRequest request) throws Exception;

    /**
     * 保存用户信息
     * @param userJSON
     * @return
     */
    User saveWeChatUser(JSONObject userJSON) throws Exception;
}
