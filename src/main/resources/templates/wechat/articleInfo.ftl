<!DOCTYPE html>
<html>
<head>
    <base href="${basePath}">
    <meta charset="utf-8">
    <title>${weChatTitle}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="mobile/css/jquery.mobile-1.4.5.min.css">
    <link href="plugins/BlogDirectory/BlogDirectory.css" rel="stylesheet">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link rel="stylesheet" href="${basePath + urls.getForLookupPath('/mobile/css/mobile-style.css')}">
</head>
<body>
<div data-role="page" id="page">
    <div data-role="main" class="ui-content">
        <h2 style="color: #38c">
        ${article.articleTitle!}
        </h2>
        <p>
            <strong>${article.categoryName!} </strong>
            <span><#if article.addTime??>${article.addTime?datetime}</#if></span>
        </p>
        <div id="article-directory"></div>
        <div id="article-content">
        ${article.articleContent!}
        </div>
    </div>
    <div data-role="footer">
        <div data-role="navbar" data-iconpos="left" class="menu">
            <ul>
                <li><a href="${basePath}weChat/article" data-icon="home" data-ajax="false">主页</a></li>
                <li><a href="${basePath}weChat/category" data-icon="bullets" data-ajax="false">索引</a></li>
                <li><a href="${basePath}weChat/suggest" data-icon="mail" data-ajax="false">反馈</a></li>
            </ul>
        </div>
    </div>
</div>
<div id="small-chat">
    <a class="open-small-chat" onclick="page_top()">
        <i class="fa fa-chevron-up"></i>
    </a>
</div>
<!-- jQuery -->
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="mobile/js/jquery.mobile-1.4.5.min.js"></script>
<script src="plugins/BlogDirectory/BlogDirectory.js"></script>
<script src="plugins/highlight/jquery.highlight.js"></script>
<script src="${basePath + urls.getForLookupPath('/js/function.js')}"></script>

<script>
    $(function () {
        /*页面加载完成之后生成博客目录*/
        BlogDirectory.showDirectoryList("article-content", "h2", "h3", 20);

        /**
         * 标红关键字
         */
        var keyWord = '${articleContent!}';
        if ($.trim(keyWord).length > 0) {
            $('#article-content').highlight(keyWord);
        }
    });
</script>
</body>
</html>
