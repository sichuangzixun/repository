<!DOCTYPE html>
<html>
<head>
    <base href="${basePath}">
    <meta charset="utf-8">
    <title>知识库管理后台系统</title>
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="hplus/css/animate.min.css" rel="stylesheet">
    <link href="hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
    <link href="hplus/css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="hplus/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
    <link href="hplus/css/plugins/jsTree/style.min.css" rel="stylesheet">
</head>
<body class="gray-bg top-navigation" id="page">
<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">
        <div class="wrapper wrapper-content">
            <div class="container min-height">
                <div class="row">
                    <div class="col-sm-12">

                        <ol class="breadcrumb">
                            <li><a href="web/article"> 首页</a></li>
                            <li><a href="web/suggest/"> 反馈建议</a></li>
                            <li>
                                <strong>回复</strong>
                            </li>
                        </ol>
                        <#--<div class="pull-right" style="margin-top: -55px">
                            <a class="btn btn-white btn-sm" href="javascript:history.back();">
                                返回
                            </a>
                        </div>-->
                        <div class="mail-body">

                            <form class="form-horizontal" method="post" id="inputForm">
                                <div class="form-group">
                                    <label class="control-label"></label>
                                    <div class="col-sm-10">
                                       <#if suggest.addTime??>
                                           ${suggest.addTime?datetime}
                                       </#if>
                                        <span class="text-success m-l-sm">${suggest.addUserName!}</span>
                                        <p>
                                        ${suggest.suggestContent!}
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">回复信息：</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="replyMessage" style="height: 150px;"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">回复人：</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="replyUserName">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"></label>
                                    <div class="col-sm-10">
                                        <button type="button" class="btn btn-primary" onclick="save()"> 保存</button>
                                        <input type="hidden" name="suggestId" value="${suggest.suggestId!}">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="small-chat">
    <a class="open-small-chat" onclick="page_top()">
        <i class="glyphicon glyphicon-menu-up"></i>
    </a>
</div>
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="hplus/js/bootstrap.min.js?v=3.3.6"></script>
<script src="hplus/js/content.min.js?v=1.0.0"></script>
<script src="${basePath + urls.getForLookupPath('/js/function.js')}"></script>
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script>

    var successCode = "${successCode}";
    var basePath = "${basePath}";


    var save = function () {
        if (valid_form()) {
            var vPost = $("#inputForm").serialize();
            var url = 'suggest/api/update';
            $.post(url, vPost, function (data) {
                responseData(data, basePath);
                if (data.code == successCode) {
                    alert(data.msg);
                    var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                    parent.layer.close(index); //再执行关闭
                }
            });
        }

    };

    function valid_form() {
        if ($.trim($("textarea[name='replyMessage']").val()) == '') {
            alert("回复信息不能为空！");
            return false;
        }
        if ($.trim($("input[name='replyUserName']").val()) == '') {
            alert("回复人姓名不能为空！");
            return false;
        }

        return true;
    }
</script>
</body>
</html>
