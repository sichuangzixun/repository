package com.sichuang.repository.controller.web;

import com.sichuang.repository.controller.BaseController;
import com.sichuang.repository.model.entity.User;
import com.sichuang.repository.other.EnumManage;
import com.sichuang.repository.service.IUserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author xs
 * @date 2018/8/1 11:39
 */
@Controller
@RequestMapping("/web/user")
public class WebUserController extends BaseController {
    @Resource
    private IUserService userService;

    @RequestMapping("/changePassword")
    public String changePassword(Model model, HttpServletRequest request) {
        User user = getUserSession(request);
        model.addAttribute("loginName", user.getLoginName());
        return "admin/user/changePassword";
    }

    //用户退出登录
    @RequestMapping("/logout")
    public String logout(Model model, HttpServletRequest request) {
        Map<String, Object> map = userService.logout(request);
        model.addAllAttributes(map);
        return "admin/login";
    }

    @RequestMapping("/list")
    public String list(Model model, HttpServletRequest request) {
        model.addAttribute("userType", EnumManage.UserTypeEnum.getList());
        return "admin/user/users";
    }


}