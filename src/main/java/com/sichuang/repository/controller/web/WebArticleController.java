package com.sichuang.repository.controller.web;

import com.sichuang.repository.controller.BaseController;
import com.sichuang.repository.model.entity.Article;
import com.sichuang.repository.model.entity.Page;
import com.sichuang.repository.model.entity.User;
import com.sichuang.repository.service.IArticleService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author xs
 * @date 2018/8/21 11:16
 */
@Controller
@RequestMapping("/web/article")
public class WebArticleController extends BaseController{

    @Resource private IArticleService articleService;

    /**
     * 首页显示文章列表。按分类查看或按标题、全文搜索
     * @param request
     * @param model
     * @return
     */
    @RequestMapping("")
    public String index(HttpServletRequest request, Model model, Article article, Page page) {
        User userSession = getUserSession(request);
        model.addAttribute(userSession);
        Map<String, Object> map = articleService.search(article, "", page);
        model.addAllAttributes(map);
        model.addAttribute(article);
        return "admin/index";
    }

    @RequestMapping("/list")
    public String list(HttpServletRequest request, Model model, Article article, Page page) {
        User userSession = getUserSession(request);
        model.addAttribute(userSession);
        article.setIsDelete(false);
        Map<String, Object> map = articleService.search(article, null, page);
        model.addAllAttributes(map);
        model.addAttribute("article", article);
        return "admin/pages/articles";
    }


    @RequestMapping("/add")
    public String add(HttpServletRequest request, Model model) {
        return "admin/pages/articleAdd";
    }

    @RequestMapping("/update")
    public String update(HttpServletRequest request, Model model, Integer articleId) throws Exception {
        Article article = articleService.verifyArticle(articleId);
        model.addAttribute(article);
        return "admin/pages/articleUpdate";
    }

    @RequestMapping("/view")
    public String view(HttpServletRequest request, Model model, Integer articleId, String articleContent) throws Exception {
        Article article = articleService.verifyArticle(articleId);
        model.addAttribute(article);
        model.addAttribute("articleContent", articleContent);
        return "admin/pages/articleInfo";
    }
}
