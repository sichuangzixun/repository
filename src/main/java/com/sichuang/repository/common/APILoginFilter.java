package com.sichuang.repository.common;

import com.alibaba.fastjson.JSON;
import com.sichuang.repository.controller.BaseController;
import com.sichuang.repository.model.entity.User;
import com.sichuang.repository.other.EnumManage;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 登录过滤器
 *
 * @author qw
 */
public class APILoginFilter implements Filter {

    @Override
    public void destroy() {
        // TODO Auto-generated method stub

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        // TODO Auto-generated method stub
        // 获得在下面代码中要用的request,response,session对象
        HttpServletRequest servletRequest = (HttpServletRequest) request;
        HttpServletResponse servletResponse = (HttpServletResponse) response;

        // 获得用户请求的URI
        String path = servletRequest.getRequestURI();

        // 登陆页面无需过滤
        if (path.indexOf("/login") > -1 || path.indexOf("/api") == -1) {
            chain.doFilter(servletRequest, servletResponse);
            return;
        }
        System.out.println(path);
        User userSession = BaseController.getUserSession(servletRequest);
        String msg = "";
        // 判断如果没有取到用户信息
        if (userSession == null) {
            // 返回未登录信息
            msg = JSON.toJSONString(MyResponse.getNotLoginResponse(), MyResponse.features);
        }else {
            if (userSession.getUserType().equals(EnumManage.UserTypeEnum.Administrator.getId()) == false){
                msg = JSON.toJSONString("您没有访问权限！", MyResponse.features);
            }else {
                // 已经登陆,继续此次请求
                chain.doFilter(request, response);
                return;
            }
        }
        // 返回未登录信息
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        servletResponse.getWriter().print(msg);
        return;

    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // TODO Auto-generated method stub
    }

}
