package com.sichuang.repository.service;

import com.alibaba.fastjson.JSONArray;
import com.sichuang.repository.model.entity.Category;
import com.sichuang.repository.other.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author xs
 * @date 2018/8/21 15:20
 */
public interface ICategoryService {


    /**
     * 获取全部分类
     * @param isArticle 是否读取文章列表
     * @return
     */
    ArrayList<TreeNode> getTree( boolean isArticle);

    /**
     * 获取下级节点
     * @param categoryId
     * @param isArticle 是否读取文章列表
     * @return
     */
    ArrayList<TreeNode> getChildrenByParentId(int categoryId, boolean isArticle);

    /**
     * 获取分类
     * @param categoryId
     * @return
     */
    List<Category> getByParentId(int categoryId);

    /**
     * 添加分类
     * @param category
     * @return
     */
    Map<String, Object> add(Category category) throws Exception;

    /**
     * 修改分类
     * @param category
     * @return
     * @throws Exception
     */
    Map<String, Object> update(Category category) throws Exception;

    /**
     * 删除分类
     * @param categoryId
     * @return
     * @throws Exception
     */
    Map<String, Object> remove(Integer categoryId) throws Exception;

    /**
     * 验证分类
     * @param categoryId
     * @return
     */
    Category verifyCategory(Integer categoryId) throws Exception;

    /**
     * 添加、修改验证输入
     * @param category
     * @return
     */
    Category verifyData(Category category) throws Exception;
}
