package com.sichuang.repository.controller.api;

import com.sichuang.repository.common.FileUtils;
import com.sichuang.repository.config.BaseConfig;
import com.sichuang.repository.controller.BaseController;
import com.sichuang.repository.model.entity.Article;
import com.sichuang.repository.model.entity.Page;
import com.sichuang.repository.model.entity.User;
import com.sichuang.repository.service.IArticleService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author xs
 * @date 2018/8/22 15:10
 */
@RestController
@RequestMapping("/api/article")
public class ArticleController extends BaseController {

    @Resource private IArticleService articleService;

    @RequestMapping("/add")
    public Map<String, Object> add(HttpServletRequest request, Article article) throws Exception {
        return articleService.add(article);
    }

    @RequestMapping("/update")
    public Map<String, Object> update(HttpServletRequest request, Article article, String imgSrc) throws Exception {
        return articleService.update(request, article, imgSrc);
    }

    @RequestMapping("/remove")
    public Map<String, Object> remove(HttpServletRequest request, String articleIds) throws Exception {
        return articleService.removeByArticleIds(articleIds);
    }

    @RequestMapping("/top")
    public Map<String, Object> top(HttpServletRequest request, String articleIds) throws Exception {
        return articleService.topByArticleIds(articleIds);
    }

    @RequestMapping("/uploadImg")
    public Map<String, Object> uploadImg(HttpServletRequest request) {
        User userSession = getUserSession(request);
        return FileUtils.pngUpload(request,userSession.getUserId().toString(),"article/images/");
    }

    @RequestMapping("/deleteImg")
    public Map<String, Object> deleteImg(HttpServletRequest request, String imgSrc) {
        imgSrc = imgSrc.replace(BaseController.getBasePath(request) + BaseConfig.uploadFolderUrl, "");
        return FileUtils.deleteFile(imgSrc);
    }
}
