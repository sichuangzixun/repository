package com.sichuang.repository.other;

import java.util.LinkedHashMap;
import java.util.Map;

public class EnumManage {

    /**
     * 用户类型：1-系统管理员、2-普通用户
     */
    public static enum UserTypeEnum {
        Administrator("系统管理员", 1),Normal("普通用户", 2);
        // 成员变量
        private String name;
        private int id;

        // 构造方法
        private UserTypeEnum(String name, int id) {
            this.name = name;
            this.id = id;
        }


        /**
         * 获取全部状态列表
         *
         * @return
         */
        public static Map<Integer, String> getList() {
            Map<Integer, String> map = new LinkedHashMap<>();
            for (UserTypeEnum e : UserTypeEnum.values()) {
                map.put(e.getId(), e.getName());
            }
            return map;
        }

        public static Map<Integer, String> getUserList() {
            Map<Integer, String> map = UserTypeEnum.getList();
            map.remove(Administrator.id);
            return map;
        }

        public String getName() {
            return name;
        }

        public int getId() {
            return id;
        }
    }


    /**
     * 布尔类型
     */
    public static enum BooleanTypeEnum {
        False("否", false), True("是", true);
        // 成员变量
        private String name;
        private boolean id;

        // 构造方法
        private BooleanTypeEnum(String name, boolean id) {
            this.name = name;
            this.id = id;
        }


        /**
         * 获取全部状态列表
         *
         * @return
         */
        public static Map<Boolean, String> getList() {
            Map<Boolean, String> map = new LinkedHashMap<>();
            for (BooleanTypeEnum e : BooleanTypeEnum.values()) {
                map.put(e.getId(), e.getName());
            }
            return map;
        }

        public String getName() {
            return name;
        }

        public boolean getId() {
            return id;
        }
    }

    /**
     * 反馈建议处理状态
     */
    public static enum ProcessStatusEnum {
        Not("未处理", 0), Looked("已处理", 1);
        // 成员变量
        private String name;
        private int id;

        // 构造方法
        private ProcessStatusEnum(String name, int id) {
            this.name = name;
            this.id = id;
        }


        /**
         * 获取全部状态列表
         *
         * @return
         */
        public static Map<Integer, String> getList() {
            Map<Integer, String> map = new LinkedHashMap<>();
            for (ProcessStatusEnum e : ProcessStatusEnum.values()) {
                map.put(e.getId(), e.getName());
            }
            return map;
        }

        public String getName() {
            return name;
        }

        public int getId() {
            return id;
        }
    }


}
