package com.sichuang.repository.controller.web;

import com.sichuang.repository.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @author xs
 * @date 2018/8/1 11:39
 */
@Controller
public class WebIndexController extends BaseController {

    @RequestMapping("/")
    public String index(HttpServletRequest request, Model model) {
        return "redirect:web/article";
    }

    @RequestMapping("/login")
    public String login(HttpServletRequest request) {
        return "admin/login";
    }


    @RequestMapping("/message")
    public String message(Model model, HttpServletRequest request, String msg) {
        model.addAttribute("msg", msg);
        return "admin/message";
    }

}
