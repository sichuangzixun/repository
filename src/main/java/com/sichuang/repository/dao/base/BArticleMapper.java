package com.sichuang.repository.dao.base;

import com.sichuang.repository.model.entity.Article;
import com.sichuang.repository.model.entity.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 全文搜索
 * @author xs
 * @date 2018/8/28 11:25
 */
public interface BArticleMapper {

    /**
     * 统计搜索记录总数
     * @param keyWord
     * @param topCategoryIdList
     * @return
     */
    int countSearchRecord(@Param("keyWord") String keyWord, @Param("topCategoryIdList")List<Integer> topCategoryIdList);

    /**
     * 标题、全文搜索
     * @param keyWord
     * @param topCategoryIdList
     * @param page
     * @return
     */
    List<Article> searchRecord(@Param("keyWord")String keyWord, @Param("topCategoryIdList")List<Integer> topCategoryIdList, @Param("page")Page page);
}
