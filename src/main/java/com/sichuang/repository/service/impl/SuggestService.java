package com.sichuang.repository.service.impl;

import com.sichuang.repository.common.MyResponse;
import com.sichuang.repository.common.StringUtil;
import com.sichuang.repository.dao.SuggestMapper;
import com.sichuang.repository.model.entity.Page;
import com.sichuang.repository.model.entity.Suggest;
import com.sichuang.repository.model.entity.SuggestExample;
import com.sichuang.repository.other.EnumManage;
import com.sichuang.repository.service.ISuggestService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author xs
 * @date 2018/8/31 14:22
 */
@Service
public class SuggestService implements ISuggestService{

    @Resource
    private SuggestMapper suggestMapper;

    @Override
    public Map<String, Object> add(Suggest suggest) throws Exception {
        if(StringUtil.isEmpty(suggest.getSuggestContent())){
            throw new Exception("反馈建议不能为空！");
        }
        if(StringUtil.isEmpty(suggest.getAddUserName())){
            throw new Exception("反馈人姓名不能为空！");
        }
        suggest.setAddTime(new Date());
        suggest.setIsDelete(false);
        suggest.setIsProcessed(EnumManage.ProcessStatusEnum.Not.getId());
        suggest.setSuggestId(null);
        int count = suggestMapper.insertSelective(suggest);
        if(count != 1){
            throw new Exception("未知原因！");
        }
        return MyResponse.getSuccessResponse(suggest);
    }

    @Override
    public Map<String, Object> update(Suggest suggest) throws Exception {
        if(StringUtil.isEmpty(suggest.getReplyMessage())){
            throw new Exception("回复信息不能为空！");
        }
        if(StringUtil.isEmpty(suggest.getReplyUserName())){
            throw new Exception("回复人姓名不能为空！");
        }
        Suggest suggestInfo = verifySuggest(suggest.getSuggestId());
        suggest.setReplyTime(new Date());
        suggest.setIsProcessed(EnumManage.ProcessStatusEnum.Looked.getId());
        int count = suggestMapper.updateByPrimaryKeySelective(suggest);
        if(count != 1){
            throw new Exception("未知原因！");
        }
        return MyResponse.getSuccessResponse(suggest);
    }

    @Override
    public Suggest verifySuggest(Integer suggestId) throws Exception {
        if(suggestId == null || suggestId <= 0){
            throw new Exception("反馈建议不存在！");
        }
        Suggest suggest = suggestMapper.selectByPrimaryKey(suggestId);
        if(suggest == null || suggest.getIsDelete()){
            throw new Exception("反馈建议不存在！");
        }
        return suggest;
    }

    @Override
    public Map<String, Object> getByPage(Page page, Integer isProcessed) {
        SuggestExample example = new SuggestExample();
        SuggestExample.Criteria criteria = example.createCriteria().andIsDeleteEqualTo(false);
        if(isProcessed != null){
            criteria.andIsProcessedEqualTo(isProcessed);
        }
        example.setPage(page);
        example.setOrderByClause("add_time desc, reply_time desc");
        List<Suggest> suggestList = suggestMapper.selectByExample(example);
        page.setTotalRecords(suggestMapper.countByExample(example));
        return MyResponse.getResponse(true, page, suggestList);
    }

    @Override
    public Map<String, Object> remove(String suggestIds) throws Exception {
        if(StringUtil.isEmpty(suggestIds)){
            throw new Exception("请选择反馈建议！");
        }
        List<Integer> suggestIdList = StringUtil.parseIdList(suggestIds);
        SuggestExample example = new SuggestExample();
        example.createCriteria().andIsDeleteEqualTo(false)
                .andSuggestIdIn(suggestIdList);
        Suggest suggest = new Suggest();
        suggest.setIsDelete(true);
        int count = suggestMapper.updateByExampleSelective(suggest, example);
        return MyResponse.getResponse(count);
    }
}
