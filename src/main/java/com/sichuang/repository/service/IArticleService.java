package com.sichuang.repository.service;

import com.sichuang.repository.model.entity.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author xs
 * @date 2018/8/21 15:41
 */
public interface IArticleService {

    /**
     * 按分类获取文章
     * @param categoryId
     * @return
     */
    List<Article> getByCategoryId(int categoryId);

    /**
     * 获取搜索条件
     * @param article
     * @param topCategoryIdList
     * @return
     */
    ArticleExample getExampleByModel(Article article, List<Integer> topCategoryIdList);

    /**
     * 分页获取文章列表
     * @param article
     * @param topCategoryIdList
     * @param page
     * @return
     */
    Map<String, Object> getByModelAndPage(Article article, List<Integer> topCategoryIdList, Page page);

    /**
     * 标题、全文搜索
     * @param keyWord
     * @param topCategoryIdList
     * @param page
     * @return
     */
    Map<String, Object> searchArticle(String keyWord, List<Integer> topCategoryIdList, Page page);

    /**
     * 搜索
     * @param article
     * @param topCategoryIds 一级分类id
     * @param page
     * @return
     */
    Map<String, Object> search(Article article, String topCategoryIds, Page page);


    /**
     * 添加文章
     * @param article
     * @return
     */
    Map<String, Object> add(Article article) throws Exception;

    /**
     * 修改文章内容
     * @param article
     * @return
     */
    Map<String, Object> update(HttpServletRequest request, Article article, String imgSrc) throws Exception;

    /**
     * 验证文章
     * @param articleId
     * @return
     */
    Article verifyArticle(Integer articleId) throws Exception;

    /**
     * 批量移除文章
     * @param articleIds
     * @return
     */
    Map<String, Object> removeByArticleIds(String articleIds) throws Exception;

    /**
     * 批量置顶文章
     * @param articleIds
     * @return
     * @throws Exception
     */
    Map<String, Object> topByArticleIds(String articleIds) throws Exception;

    /**
     * 验证数据
     * @param article
     * @return
     */
    Article verifyData(Article article) throws Exception;

    /**
     * 更新文章分类为其他
     * @param categoryIdList
     * @return
     */
    Map<String, Object> updateCategory(List<Integer> categoryIdList);

    /**
     * 修改分类名称，同步修改文章分类名称。
     * @param category
     * @return
     */
    Map<String, Object> updateCategoryName(Category category);
}
