package com.sichuang.repository.controller.api;

import com.sichuang.repository.controller.BaseController;
import com.sichuang.repository.model.entity.Article;
import com.sichuang.repository.model.entity.Page;
import com.sichuang.repository.service.IArticleService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author xs
 * @date 2018/8/22 15:10
 */
@RestController
@RequestMapping("/weChat")
public class WeChatApiController extends BaseController {

    @Resource private IArticleService articleService;

    /**
     * 搜索文章
     * @param request
     * @param article
     * @param categoryIds 一级分类ids
     * @param page
     * @return
     */
    @RequestMapping("/article/get")
    public Map<String, Object> get(HttpServletRequest request, Article article, String categoryIds,  Page page) {
        return articleService.search(article, categoryIds, page);
    }
}
