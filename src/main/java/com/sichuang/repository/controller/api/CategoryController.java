package com.sichuang.repository.controller.api;

import com.sichuang.repository.controller.BaseController;
import com.sichuang.repository.model.entity.Category;
import com.sichuang.repository.other.TreeNode;
import com.sichuang.repository.service.ICategoryService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author xs
 * @date 2018/8/21 15:48
 */
@RestController
@RequestMapping("/api/category")
public class CategoryController extends BaseController {

    @Resource private ICategoryService categoryService;

    /**
     * 分类树形结构
     * @param request
     * @param isArticle 是否包含文章列表。默认为否
     * @return
     */
    @RequestMapping("/tree")
    public List<TreeNode> tree(HttpServletRequest request, Boolean isArticle) {
        if(isArticle == null){
            isArticle = false;
        }
        return categoryService.getTree(isArticle);
    }

    @RequestMapping("/add")
    public Map<String, Object> add(HttpServletRequest request, Category category) throws Exception {
        return categoryService.add(category);
    }

    @RequestMapping("/update")
    public Map<String, Object> update(HttpServletRequest request, Category category) throws Exception {
        return categoryService.update(category);
    }

    @RequestMapping("/remove")
    public Map<String, Object> remove(HttpServletRequest request, Integer categoryId) throws Exception {
        return categoryService.remove(categoryId);
    }
}
