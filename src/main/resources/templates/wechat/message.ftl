<!DOCTYPE html>
<html>
<head>
    <base href="${basePath}">
    <meta charset="utf-8">
    <title>${weChatTitle}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="mobile/css/jquery.mobile-1.4.5.min.css">
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link rel="stylesheet" href="${basePath + urls.getForLookupPath('/mobile/css/mobile-style.css')}">
</head>
<body>
<div data-role="page">
    <div data-role="main" class="ui-content text-center">
        <h3>系统提示！</h3>
        <div class="text-danger">
            <label>${msg!}</label>
        </div>

    </div>
    <div data-role="footer">
        <div data-role="navbar" data-iconpos="left" class="menu">
            <ul>
                <li><a href="${basePath}weChat/article" data-icon="home" data-ajax="false">主页</a></li>
                <li><a href="${basePath}weChat/category" data-icon="bullets" data-ajax="false">索引</a></li>
                <li><a href="${basePath}weChat/suggest" data-icon="mail" data-ajax="false">反馈</a></li>
            </ul>
        </div>
    </div>
</div>

<!-- jQuery -->
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="mobile/js/jquery.mobile-1.4.5.min.js"></script>
</body>
</html>
