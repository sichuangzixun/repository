<!DOCTYPE html>
<html>
<head>
    <base href="${basePath}">
    <meta charset="utf-8">
    <title>知识库管理后台系统</title>
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="hplus/css/animate.min.css" rel="stylesheet">
    <link href="hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
    <link href="plugins/BlogDirectory/BlogDirectory.css" rel="stylesheet">
</head>
<body class="gray-bg top-navigation" id="page">
<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom white-bg">
            <div class="container">
                <nav class="navbar navbar-static-top" role="navigation">
                    <div class="navbar-header">
                        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar"
                                data-toggle="collapse"
                                class="navbar-toggle collapsed" type="button">
                            <i class="fa fa-reorder"></i>
                        </button>
                    </div>

                    <div class="navbar-collapse collapse" id="navbar">
                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="web/article/"> 首页</a>
                            </li>
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="web/article/list"> 管理文章 </a>
                            </li>
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="web/category/list"> 管理分类 </a>
                            </li>
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="web/suggest/"> 反馈建议 </a>
                            </li>
                        </ul>
                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <form role="form" class="form-inline m-sm" action="web/article/" method="post">
                                    <div class="input-group">
                                        <input type="text" placeholder="搜索标题、正文" name="articleContent"
                                               class="form-control">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-primary">搜索</button>
                                        </span>
                                    </div>
                                </form>
                            </li>
                            <li>
                                <a href="javascript:void(0);" onclick="openWindow('添加文章', 'web/article/add');" >
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                                   data-toggle="dropdown"> <i class="fa fa-user"></i> </a>
                                <ul role="menu" class="dropdown-menu">
                                    <li><a href="web/user/changePassword">修改密码</a>
                                    </li>
                                    <li><a href="web/user/logout">退出登录</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <div class="container min-height">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="web/article"> 首页</a></li>
                            <li><a href="web/article/list"> 管理文章</a></li>
                            <li>
                                <strong>查看文章</strong>
                            </li>
                        </ol>
                        <div class="pull-right" style="margin-top: -55px">
                            <button onclick="openWindow('编辑文章', 'web/article/update?articleId=${article.articleId }')"
                                    class="btn btn-success btn-sm">
                                <i class="glyphicon glyphicon-edit"></i>
                                编辑
                            </button>
                            <button id="btn_remove" class="btn btn-danger btn-sm">
                                <i class="glyphicon glyphicon-trash"></i> 删除
                            </button>
                            <a class="btn btn-white btn-sm" href="javascript:history.back();">
                                返回
                            </a>
                        </div>
                        <div class="article-title">
                            <h2>
                            ${article.articleTitle!}
                            </h2>
                        </div>

                        <div class="small m-b-md">
                            <strong>${article.categoryName!} </strong>
                            <span class="text-muted">
                                        <#if article.addTime??> <i
                                                class="fa fa-clock-o"></i>${article.addTime?datetime}</#if>
                            </span>
                        </div>
                        <div id="article-content">
                            ${article.articleContent!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="small-chat">
    <a class="open-small-chat" onclick="page_top()">
        <i class="glyphicon glyphicon-menu-up"></i>
    </a>
</div>
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="hplus/js/bootstrap.min.js?v=3.3.6"></script>
<script src="hplus/js/content.min.js?v=1.0.0"></script>
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script src="plugins/highlight/jquery.highlight.js"></script>
<script src="plugins/BlogDirectory/BlogDirectory.js"></script>
<script src="${basePath + urls.getForLookupPath('/js/function.js')}"></script>

<script>
    $(function () {

        //2.初始化Button的点击事件
        var oButtonInit = new ButtonInit();
        oButtonInit.Init();

        /*页面加载完成之后生成博客目录*/
        BlogDirectory.showDirectorySideBar("article-content", "h2", "h3", 20);
        //搜索关键字标红
        <#if articleContent??  >
            $('#article-content').highlight('${articleContent!}');
        </#if>
    });

    var successCode = "${successCode}";
    var basePath = "${basePath}";


    function go(ids, act, word) {
        if (ids.length == 0) {
            alert("您还没有选择！");
            return;
        }
        if (!confirm(word))
            return;
        $.post("api/article/" + act + "?articleIds=" + ids, "", function (data) {
            responseData(data, basePath);
            if (data.code == successCode) {
                alert(data.msg);
                window.location.reload();
            }
        });
    }

    var ButtonInit = function () {
        var oInit = new Object();
        var postdata = {};

        oInit.Init = function () {
            //初始化页面上面的按钮事件
            $('#btn_remove').click(function () {
                var ids = '${article.articleId}';
                go(ids, "remove", "确认要移除选定的项目吗？");
            });
        };

        return oInit;
    };
</script>
</body>
</html>
