package com.sichuang.repository.controller.web;

import com.sichuang.repository.controller.BaseController;
import com.sichuang.repository.model.entity.Page;
import com.sichuang.repository.model.entity.Suggest;
import com.sichuang.repository.other.EnumManage;
import com.sichuang.repository.service.ISuggestService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/**
 * @author xs
 * @date 2018/8/31 14:43
 */
@Controller
@RequestMapping("/web/suggest")
public class WebSuggestController extends BaseController{

    @Resource
    private ISuggestService suggestService;

    @RequestMapping("")
    public String index(Model model, Page page, Integer isProcessed){
        model.addAllAttributes(suggestService.getByPage(page, isProcessed));
        model.addAttribute("isProcessed",isProcessed);
        return "admin/pages/suggests";
    }

    @RequestMapping("/update")
    public String update(Model model, Integer suggestId) throws Exception {
        Suggest suggest = suggestService.verifySuggest(suggestId);
        model.addAttribute(suggest);
        return "admin/pages/suggestUpdate";
    }
}
