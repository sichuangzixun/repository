<!DOCTYPE html>
<html>
<head>
    <base href="${basePath}">
    <meta charset="utf-8">
    <title>${weChatTitle}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link rel="stylesheet" href="mobile/css/jquery.mobile-1.4.5.min.css">
    <link rel="stylesheet" href="${basePath + urls.getForLookupPath('/mobile/css/mobile-style.css')}">
</head>
<style>
    /*显示分页信息*/
    #wrapper {
        margin-bottom: 20px;
    }
</style>
<body>
<div data-role="page" id="pageone">

    <div data-role="header">
        <form id="searchForm" method="post" action="" class="ui-filterable">
            <div class="ui-field-contain">
                <input type="text" data-type="search" name="articleContent" id="search"
                       value="${article.articleContent!}"
                       placeholder="搜索文章">
            </div>
            <input type="hidden" name="categoryIds" value="${categoryIds!}">
            <input type="hidden" name="categoryId" value="${article.categoryId!}">
        </form>
    </div>
    <div data-role="main" class="ui-content">
        <div id="wrapper">
            <ul data-role="listview" class="m" id="article_list">
                <li data-role="divider">
                    <#if article.articleContent?? && article.articleContent?length gt 0>关键字“${article.articleContent!}
                        ”</#if>
                    搜索结果</li>
            </ul>
        </div>
        <div id="page_info" style="display: none"></div>
    </div>
    <div data-role="footer">
        <div data-role="navbar" data-iconpos="left" class="menu">
            <ul>
                <li><a href="${basePath}weChat/article" data-icon="home" data-ajax="false">主页</a></li>
                <li><a href="${basePath}weChat/category" data-icon="bullets" data-ajax="false">索引</a></li>
                <li><a href="${basePath}weChat/suggest" data-icon="mail" data-ajax="false">反馈</a></li>
            </ul>
        </div>
    </div>
</div>
<!-- jQuery -->
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="mobile/js/jquery.mobile-1.4.5.min.js"></script>
<script src="plugins/highlight/jquery.highlight.js"></script>
<script src="${basePath + urls.getForLookupPath('/js/function.js')}"></script>
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script>

    var successCode = "${successCode}";
    var basePath = "${basePath}";
    var pageNo = 0;
    var pageCount = 1;
    var keyWord = '${article.articleContent!}';
    $(function () {


        $('#search').click(function () {
            var vPost = $("#searchForm").serialize();
            window.location.href = basePath + "weChat/article/search?" + vPost;
        });

        scrollPage();
        var timer = 0;
        $(window).scroll(function () {
            var scrollTop = $(this).scrollTop();
            var scrollHeight = $(document).height();
            var windowHeight = $(this).height();
            if (scrollTop + windowHeight == scrollHeight) {
                //此处是滚动条到底部时候触发的事件，在这里写要加载的数据，或者是拉动滚动条的操作
                console.log(timer);
                if (timer) {
                    clearTimeout(timer);
                }
                timer = setTimeout(function () {
                    scrollPage();
                }, 3000);
                console.log(timer);
            }

        });

    });

    /**
     * 加载下一页
     * @returns {boolean}
     */
    function scrollPage() {
        if (pageNo <= pageCount) {
            var vPost = $("#searchForm").serialize();
            var loadPage = pageNo + 1;
            var url = 'weChat/article/get?pageNo=' + loadPage;
            $.post(url, vPost, function (data) {
                responseData(data, basePath);
                if (data.code == successCode) {
                    var page = data.page;
                    pageCount = page.pageCount;
                    if (page.totalRecords == 0) {
                        showPageInfo("对不起，没有找到相关文章！");
                        return false;
                    }
                    $("#page_info").hide();
                    for (var i = 0; i < data.data.length; i++) {
                        var text = getArticleHtml(basePath, data.data[i], keyWord);
                        $(text).appendTo('#article_list');
                        $("#article_list").listview("refresh");
                    }
                    pageNo++;
                    highlightK();
                    if (loadPage >= pageCount) {
                        showPageInfo("已经到底了！");
                        return false;
                    }
                }
            });
        }

    }

    function showPageInfo(messge) {
        $("#page_info").text(messge);
        $("#page_info").show();
    }

    /**
     * 标红关键字
     */
    function highlightK() {
        if ($.trim(keyWord).length > 0) {
            $('#article_list').highlight(keyWord);
        }

    }


</script>

</body>
</html>
