<!DOCTYPE html>
<html>
<head>
    <base href="${basePath}">
    <meta charset="utf-8">
    <title>知识库管理后台系统</title>
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="hplus/css/animate.min.css" rel="stylesheet">
    <link href="hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
    <link href="plugins/summernote/summernote.css?v=0.8.9" rel="stylesheet">
    <link href="hplus/css/plugins/jsTree/style.min.css" rel="stylesheet">
</head>
<body class="gray-bg top-navigation" id="page">
<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">
        <div class="wrapper wrapper-content">
            <div class="container min-height">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="web/article"> 首页</a></li>
                            <li><a href="web/article/list"> 管理文章</a></li>
                            <li>
                                <strong>编辑文章</strong>
                            </li>
                        </ol>
                        <div class="pull-right" style="margin-top: -55px">
                            <button class="btn btn-primary btn-sm" onclick="save()"> 保存</button>
                        </div>
                        <div class="mail-body">
                            <div class="alert alert-warning">设置标题2、标题3可自动生成目录导航。</div>
                            <form class="form-horizontal" method="post" id="inputForm">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">文章标题：</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="articleTitle"
                                               value="${article.articleTitle!}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">分类：</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="categoryName"
                                               id="categoryName" value="${article.categoryName!}"
                                               onclick="showMenu(); return false;">
                                        <div id="using_json"
                                             style="position: absolute; max-height:300px;display: none;background: white;z-index: 10;"></div>
                                        <input type="hidden" name="categoryId" id="categoryId"
                                               value="${article.categoryId!}">
                                        <input type="hidden" name="articleContent" id="articleContent">
                                        <input type="hidden" name="articleId" id="articleId"
                                               value="${article.articleId}">
                                        <input type="hidden" name="imgSrc" id="imgSrc"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="mail-text h-200">
                            <div class="summernote">
                            ${article.articleContent}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="small-chat">
    <a class="open-small-chat" onclick="page_top()">
        <i class="glyphicon glyphicon-menu-up"></i>
    </a>
</div>
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="hplus/js/bootstrap.min.js?v=3.3.6"></script>
<script src="hplus/js/content.min.js?v=1.0.0"></script>
<script src="${basePath + urls.getForLookupPath('/js/function.js')}"></script>
<script src="plugins/summernote/summernote.min.js?v=0.8.9"></script>
<script src="plugins/summernote/lang/summernote-zh-CN.js"></script>
<script src="hplus/js/plugins/jsTree/jstree.min.js"></script>
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<style>
    #using_json {
        max-width: 100%;
        overflow: auto;
        font: 10px Verdana, sans-serif;
        box-shadow: 0 0 5px #ccc;
        padding: 10px;
        border-radius: 5px;
        z-index: 1000 !important;
    }
</style>
<script>
    $(function () {
        $('.summernote').summernote({
            height: 600,
            lang: 'zh-CN',
            focus: true,
            callbacks: {
                // onImageUpload callback
                onImageUpload: function (files) {
                    // upload image to server and create imgNode...
                    var data = new FormData();
                    data.append("file", files[0]);
                    $.ajax({
                        data: data,
                        type: "POST",
                        url: "api/article/uploadImg", //图片上传出来的url，返回的是图片上传后的路径，http格式
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: "json",
                        success: function (data) {//data是返回的hash,key之类的值，key是定义的文件名
                            console.log(data.data);
                            $('.summernote').summernote('insertImage', "${urlPath}" + data.data[0], 'img');
                        },
                        error: function () {
                            alert("上传失败");
                        }
                    });
                },
                onMediaDelete: function (target) {
                    //保存删除图片路径
                    var imgSrc = target.context.currentSrc;
                    var img = $('#imgSrc').val();
                    imgSrc = img == "" ? imgSrc : img + "," + imgSrc;
                    $('#imgSrc').val(imgSrc);
                }
            }
        });
        $('#using_json').jstree({
            "core": {
                "animation": 0,
                "check_callback": true,//true可修改树结构
                'force_text': true,//escape HTML
                'multiple': false,
                'data': {
                    'url': 'api/category/tree'
                }
            },
            "types": {
                "default": {
                    "valid_children": ["default", "file"]
                },
                "file": {
                    "icon": "glyphicon glyphicon-file",
                    "valid_children": []
                }
            },
            "conditionalselect": function (node, event) {
                $('#categoryName').val(node.text);
                $('#categoryId').val(node.id);
                hideMenu();
                return false;
            },
            "plugins": [
                "dnd", "search", "types", "wholerow", "conditionalselect"/*"contextmenu",*/
            ]
        });

        $("#using_json").on("loaded.jstree", function (event, data) {
            // 展开所有节点
            $('#using_json').jstree('open_all');
        });

        //搜索分类
        var to = false;
        $('#categoryName').keyup(function () {
            if (to) {
                clearTimeout(to);
            }
            to = setTimeout(function () {
                var v = $('#categoryName').val();
                $('#using_json').jstree(true).search(v);
            }, 250);
        });

    });

    var successCode = "${successCode}";
    var basePath = "${basePath}";

    function showMenu() {
        $("#using_json").slideDown("fast");
        setTimeout(function () {
            hideMenu();
        }, 4000);
    }

    function hideMenu() {
        $("#using_json").fadeOut("fast");
    }

    var save = function () {
        if (valid_form()) {
            layer.load(0, {shade: false}); //0代表加载的风格，支持0-2
            var vPost = $("#inputForm").serialize();
            var url = 'api/article/update';
            $.post(url, vPost, function (data) {
                responseData(data, basePath);
                if (data.code == successCode) {
                    alert(data.msg);
                    var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                    parent.layer.close(index); //再执行关闭
                }
            });
        }

    };

    function valid_form() {
        if ($.trim($("input[name='articleTitle']").val()) == '') {
            alert("文章标题不能为空！");
            return false;
        }
        if ($.trim($("input[name='categoryName']").val()) == '') {
            alert("分类不能为空！");
            return false;
        }
        var aHTML = $(".summernote").summernote('code');
        $("#articleContent").val(aHTML);
        return true;
    }
</script>
</body>
</html>
