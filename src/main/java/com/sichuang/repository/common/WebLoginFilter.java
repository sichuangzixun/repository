package com.sichuang.repository.common;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sichuang.repository.model.entity.AccessRecord;
import com.sichuang.repository.model.entity.User;
import com.sichuang.repository.controller.BaseController;
import com.sichuang.repository.other.EnumManage;
import com.sichuang.repository.service.IAccessRecordService;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

/**
 * 登录过滤器
 *
 * @author qw
 */
public class WebLoginFilter implements Filter {

    private IAccessRecordService accessRecordService;

    @Override
    public void destroy() {
        // TODO Auto-generated method stub

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        // TODO Auto-generated method stub
        // 获得在下面代码中要用的request,response,session对象
        HttpServletRequest servletRequest = (HttpServletRequest) request;
        HttpServletResponse servletResponse = (HttpServletResponse) response;

        // 获得用户请求的URI
        String path = servletRequest.getRequestURI();

        // 获取请求的类型 0：无需登录 1：需要登录
        User userSession = BaseController.getUserSession(servletRequest);
        if (accessRecordService == null) {
            BeanFactory factory = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getServletContext());
            accessRecordService = (IAccessRecordService) factory.getBean("accessRecordService");
        }
        // 访问静态文件不保存
        if (path.indexOf("/weChat") > -1 || path.indexOf("/api") > -1 ) {
            if(path.indexOf("/message") == -1){
                try {
                    // 保存访问记录
                    accessRecordService.add(servletRequest);
                }catch (Exception e){
                    System.out.println(e.getMessage());
                }
            }
        }

        // 登陆页面和微信端无需过滤
        if (path.indexOf("/login") > -1 || path.indexOf("/web") == -1) {
            chain.doFilter(servletRequest, servletResponse);
            return;
        }
        System.out.println(path);

        // 判断如果没有取到用户信息
        if (userSession == null) {
            servletResponse.sendRedirect(BaseController.LoginUrl);
            return;
        }
        String message = "";
        //普通用户不能登录管理后台
        if (userSession.getUserType().equals(EnumManage.UserTypeEnum.Normal.getId())) {
            message = URLEncoder.encode("普通用户不能登录管理后台！", "UTF-8");
        } else {
            // 已经登陆,继续此次请求
            chain.doFilter(request, response);
            return;
        }

        servletResponse.sendRedirect(BaseController.AdminMsgUrl + "msg=" + message);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // TODO Auto-generated method stub

    }

}
