package com.sichuang.repository.controller.web;

import com.sichuang.repository.controller.BaseController;
import com.sichuang.repository.model.entity.Category;
import com.sichuang.repository.model.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @author xs
 * @date 2018/8/21 11:20
 */
@Controller
@RequestMapping("/web/category")
public class WebCategoryController extends BaseController{

    @RequestMapping("/list")
    public String list(HttpServletRequest request, Model model) {
        User userSession = getUserSession(request);
        model.addAttribute(userSession);
        return "admin/pages/categories";
    }
}
