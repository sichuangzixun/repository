package com.sichuang.repository.controller.api;

import com.sichuang.repository.model.entity.Suggest;
import com.sichuang.repository.service.ISuggestService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author xs
 * @date 2018/8/31 14:53
 */
@RestController
@RequestMapping("/suggest")
public class SuggestController {

    @Resource
    private ISuggestService suggestService;

    @RequestMapping("/add")
    Map<String, Object> add(Suggest suggest) throws Exception {
        return suggestService.add(suggest);
    }

    @RequestMapping("/api/update")
    Map<String, Object> update(Suggest suggest) throws Exception {
        return suggestService.update(suggest);
    }

    @RequestMapping("/api/remove")
    Map<String, Object> remove(String suggestIds) throws Exception {
        return suggestService.remove(suggestIds);
    }

}
