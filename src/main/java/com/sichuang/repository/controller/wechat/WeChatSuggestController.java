package com.sichuang.repository.controller.wechat;

import com.sichuang.repository.controller.BaseController;
import com.sichuang.repository.model.entity.Page;
import com.sichuang.repository.service.ISuggestService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/**
 * @author xs
 * @date 2018/8/31 14:41
 */
@Controller
@RequestMapping("/weChat/suggest")
public class WeChatSuggestController extends BaseController{

    @Resource
    private ISuggestService suggestService;

    @RequestMapping("")
    public String add(Model model){
        return "wechat/suggestAdd";
    }

    @RequestMapping("/list")
    public String list(Model model, Page page, Integer processStatus){
        model.addAllAttributes(suggestService.getByPage(page, processStatus));
        return "wechat/suggests";
    }

}
