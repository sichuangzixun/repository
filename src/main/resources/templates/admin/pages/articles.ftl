<!DOCTYPE html>
<html>
<head>
    <base href="${basePath}">
    <meta charset="utf-8">
    <title>知识库管理后台系统</title>
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="hplus/css/animate.min.css" rel="stylesheet">
    <link href="hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
    <link href="hplus/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="hplus/css/plugins/jsTree/style.min.css" rel="stylesheet">
</head>
<body class="gray-bg top-navigation" id="page">

<div id="wrapper">

    <div id="page-wrapper" class="gray-bg">

        <div class="row border-bottom white-bg">
            <div class="container">
                <nav class="navbar navbar-static-top" role="navigation">
                    <div class="navbar-header">
                        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar"
                                data-toggle="collapse"
                                class="navbar-toggle collapsed" type="button">
                            <i class="fa fa-reorder"></i>
                        </button>
                    </div>

                    <div class="navbar-collapse collapse" id="navbar">
                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="web/article/"> 首页</a>
                            </li>
                            <li class="active">
                                <a aria-expanded="false" role="button" href="web/article/list"> 管理文章 </a>
                            </li>
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="web/category/list"> 管理分类 </a>
                            </li>
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="web/suggest/"> 反馈建议 </a>
                            </li>
                        </ul>
                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <form role="form" class="form-inline m-sm" action="web/article/" method="post">
                                    <div class="input-group">
                                        <input type="text" placeholder="搜索标题、正文" name="articleContent" class="form-control">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-primary">搜索</button>
                                        </span>
                                    </div>
                                </form>
                            </li>
                            <li>
                                <a href="javascript:void(0);" onclick="openWindow('添加文章', 'web/article/add');" >
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                                   data-toggle="dropdown"> <i class="fa fa-user"></i> </a>
                                <ul role="menu" class="dropdown-menu">
                                    <li><a href="web/user/changePassword">修改密码</a>
                                    </li>
                                    <li><a href="web/user/logout">退出登录</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <div class="container min-height">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="web/article"> 首页</a></li>
                            <li>
                                <strong>管理文章</strong>
                            </li>
                        </ol>
                            <form id="searchForm" class="form-inline " role="form" action="web/article/list"
                                  method="post">
                                <div class="form-group">
                                    <label class="control-label">分类：</label>
                                    <input type="text" class="form-control" id="categoryName" value=""
                                           onclick="showMenu(); return false;">
                                    <div id="using_json"
                                         style="position: absolute; max-height:300px;display: none;background: white;z-index: 10;"></div>
                                    <input type="hidden" name="categoryId" id="categoryId">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">标题：</label>
                                    <input type="text" name="articleTitle" class="form-control" value="${article.articleTitle!}"/>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">正文：</label>
                                    <input type="text" name="articleContent" class="form-control" value="${article.articleContent!}" placeholder="搜索标题、正文"/>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="begin" id="begin" value="${page.begin}"/>
                                    <input type="hidden" name="length" id="length" value="${page.length}"/>
                                    <button type="submit" id="btn_query" class="btn btn-success">搜索</button>
                                    <button type="reset" class="btn btn-white">清空</button>
                                </div>
                            </form>

                            <div class="mail-tools tooltip-demo m-sm">
                                <div class="btn-group pull-right">
                                    <span class="pull-left small text-muted m">显示第 <#if page.end gt 0>${page.begin + 1}<#else>${page.begin}</#if> 到第 ${page.end} 条记录，总共 ${page.totalRecords} 条记录</span>

                                    <button id="btn_last" class="btn btn-white btn-sm">
                                        <i class="fa fa-arrow-left"></i>
                                    </button>
                                    <button id="btn_next" class="btn btn-white btn-sm">
                                        <i class="fa fa-arrow-right"></i>
                                    </button>
                                </div>
                                <button id="btn_refresh" class="btn btn-white btn-sm">
                                    <i class="glyphicon glyphicon-refresh"></i>
                                </button>
                                <button id="btn_add" class="btn btn-success btn-sm">
                                    <i class="glyphicon glyphicon-plus"></i> 添加
                                </button>
                                <button id="btn_top" class="btn btn-warning btn-sm">
                                    <i class="fa fa-arrow-up"></i> 置顶
                                </button>
                                <button id="btn_remove" class="btn btn-danger btn-sm">
                                    <i class="glyphicon glyphicon-trash"></i> 删除
                                </button>
                            </div>
                            <table class="table table-hover table-mail" id="projectTable">
                                <tbody>
                                <tr class="unread">
                                    <td class="check-mail">
                                        <input type="checkbox" class="i-checks" id="select_all" value="1">
                                    </td>
                                    <td>分类</td>
                                    <td class="">文章标题</td>
                                    <td class="">添加时间</td>
                                    <td class="">更新时间</td>
                                    <td class=""></td>
                                </tr>
                                <#list data as article>
                                    <tr class="">
                                        <td class="check-mail">
                                            <input type="checkbox" class="i-checks" name="articleId[]"
                                                   value="${article.articleId }">
                                        </td>
                                        <td>
                                            ${article.categoryName}
                                        </td>
                                        <td class="project-title">
                                            <#if article.isTop><strong class="text-danger">[置顶]</strong></#if>
                                            <a href="web/article/view?articleId=${article.articleId }">
                                                ${article.articleTitle}
                                            </a>
                                        </td>
                                        <td><#if article.addTime??>${article.addTime?datetime}</#if></td>
                                        <td><#if article.updateTime??>${article.updateTime?datetime}</#if></td>
                                        <td class="project-actions">
                                            <a href="web/article/view?articleId=${article.articleId }&articleContent=${article.articleContent!}"
                                               class="btn btn-white btn-sm">
                                                <i class="fa fa-folder"></i>
                                                查看
                                            </a>
                                            <button onclick="openWindow('编辑文章', 'web/article/update?articleId=${article.articleId }')"
                                               class="btn btn-white btn-sm">
                                                <i class="glyphicon glyphicon-edit"></i>
                                                编辑
                                            </button>
                                        </td>
                                    </tr>
                                </#list>
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="pull-right">
                &copy; 2018
                <a href="#" target="_blank">思创智汇（广州）科技有限公司</a>
            </div>
        </div>
    </div>
</div>
<div id="small-chat">
    <a class="open-small-chat" onclick="page_top()">
        <i class="glyphicon glyphicon-menu-up"></i>
    </a>
</div>
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="hplus/js/bootstrap.min.js?v=3.3.6"></script>
<script src="hplus/js/content.min.js?v=1.0.0"></script>
<script src="${basePath + urls.getForLookupPath('/js/function.js')}"></script>
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script src="hplus/js/plugins/iCheck/icheck.min.js"></script>
<script src="hplus/js/plugins/jsTree/jstree.min.js"></script>
<script src="plugins/highlight/jquery.highlight.js"></script>
<style>
    #using_json {
        max-width: 100%;
        overflow: auto;
        font: 10px Verdana, sans-serif;
        box-shadow: 0 0 5px #ccc;
        padding: 10px;
        border-radius: 5px;
    }
</style>
<script>

    $(function () {
        //分类
        $('#using_json').jstree({
            "core": {
                "animation": 0,
                "check_callback": true,//true可修改树结构
                'force_text': true,//escape HTML
                'multiple': false,
                'data': {
                    'url': 'api/category/tree'
                }
            },
            "types": {
                "default": {
                    "valid_children": ["default", "file"]
                },
                "file": {
                    "icon": "glyphicon glyphicon-file",
                    "valid_children": []
                }
            },
            "conditionalselect": function (node, event) {
                $('#categoryName').val(node.text);
                $('#categoryId').val(node.id);
                hideMenu();
                return false;
            },
            "plugins": [
                "dnd", "search", "types", "wholerow", "conditionalselect"/*"contextmenu",*/
            ]
        });

        $("#using_json").on("loaded.jstree", function (event, data) {
            // 展开所有节点
            $('#using_json').jstree('open_all');
            // 选中指定节点
            init_categoryId('${article.categoryId!}');
        });

        $(".i-checks").iCheck({checkboxClass: "icheckbox_square-green", radioClass: "iradio_square-green",});

        //2.初始化Button的点击事件
        var oButtonInit = new ButtonInit();
        oButtonInit.Init();

        //搜索关键字标红
        <#if article.articleContent??  >
            $('#projectTable').highlight('${article.articleContent!}');
        </#if>
    });

    var successCode = "${successCode}";
    var basePath = "${basePath}";

    function getIdSelections() {
        var spCodesTemp = "";
        $("input:checkbox[name='articleId[]']:checked").each(function (i) {
            if (0 == i) {
                spCodesTemp = $(this).val();
            } else {
                spCodesTemp += ("," + $(this).val());
            }
        });
        return spCodesTemp;
    }

    function go(ids, act, word) {
        if (ids.length == 0) {
            alert("您还没有选择！");
            return;
        }
        if (!confirm(word))
            return;
        $.post("api/article/" + act + "?articleIds=" + ids, "", function (data) {
            responseData(data, basePath);
            if (data.code == successCode) {
                alert(data.msg);
                window.location.reload();
            }
        });
    }
    //显示分类
    function showMenu() {
        $("#using_json").slideDown("fast");
        setTimeout(function () {
            hideMenu();
        }, 4000);
    }
    //隐藏分类
    function hideMenu() {
        $("#using_json").fadeOut("fast");
    }
    //搜索分类
    function init_categoryId(categoryId) {
        var ref = $('#using_json').jstree(true);
        var node = ref.get_node(categoryId);
        console.log(node);
        $('#categoryName').val(node.text);
        $('#categoryId').val(categoryId);
    }

    var ButtonInit = function () {
        var oInit = new Object();
        var postdata = {};
        var totalRecords = '${page.totalRecords}';
        var to;

        oInit.Init = function () {
            //初始化页面上面的按钮事件
            $('#btn_remove').click(function () {
                var ids = getIdSelections();
                go(ids, "remove", "确认要移除选定的项目吗？");
            });

            $('#btn_top').click(function () {
                var ids = getIdSelections();
                go(ids, "top", "确认要置顶选定的项目吗？");
            });

            $('#btn_refresh').click(function () {
                window.location.reload();
            });

            $('#btn_next').click(function () {
                var length =  parseInt($("#length").val());
                var begin = parseInt($("#begin").val()) + length;
                if (begin >= totalRecords) {
                    alert("当前页面为最后一页！");
                    return false;
                }
                $("#begin").val(begin);
                $("#searchForm").submit();
            });

            $('#btn_last').click(function () {
                var begin = parseInt($("#begin").val());
                var length =  parseInt($("#length").val());
                if (begin == 0 ) {
                    alert("当前页面为第一页！");
                    return false;
                }
                $("#begin").val(begin - length);
                $("#searchForm").submit();
            });

            $('#select_all').on('ifChecked', function (event) {
                $("input").iCheck('check');
            });
            $('#select_all').on('ifUnchecked', function (event) {
                $("input").iCheck('uncheck');
            });

            //add按钮绑定查询事件
            $('#btn_add').click(function () {
                openWindow("添加文章", "web/article/add");
            });

            $('#btn_query').click(function () {
                $("#begin").val(0);
            });

            //搜索
            $('#categoryName').keyup(function () {
                if (to) {
                    clearTimeout(to);
                }
                to = setTimeout(function () {
                    var v = $('#categoryName').val();
                    $('#using_json').jstree(true).search(v);
                }, 250);
            });

        };

        return oInit;
    };
</script>
<style>
    #projectTable {
        min-width: 1000px;
        padding-bottom: 150px;
    }
</style>
</body>
</html>
