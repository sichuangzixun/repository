package com.sichuang.repository.controller.wechat;

import com.alibaba.fastjson.JSON;
import com.sichuang.repository.controller.BaseController;
import com.sichuang.repository.model.entity.Article;
import com.sichuang.repository.model.entity.Category;
import com.sichuang.repository.model.entity.Page;
import com.sichuang.repository.model.entity.SearchKeyWord;
import com.sichuang.repository.service.IArticleService;
import com.sichuang.repository.service.ICategoryService;
import com.sichuang.repository.service.ISearchKeyWordService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author xs
 * @date 2018/8/29 9:53
 */
@Controller
@RequestMapping("/weChat/article")
public class WeChatArticleController extends BaseController {

    @Resource
    private IArticleService articleService;
    @Resource
    private ICategoryService categoryService;
    @Resource
    private ISearchKeyWordService searchKeyWordService;

    @RequestMapping("")
    public String index(HttpServletRequest request, Model model) {
        List<Category> categoryList = categoryService.getByParentId(0);
        model.addAttribute(categoryList);
        return "wechat/index";
    }

    @RequestMapping("/search")
    public String search(HttpServletRequest request, Model model, Article article, String categoryIds) {
        List<Category> categoryList = categoryService.getByParentId(0);
        model.addAttribute(categoryList);
        model.addAttribute("article",article);
        model.addAttribute("categoryIds",categoryIds);
        List<SearchKeyWord> searchKeyWordList = searchKeyWordService.getTopSearch();
        model.addAttribute("searchKeyWordList", searchKeyWordList);
        return "wechat/search";
    }

    @RequestMapping("/list")
    public String list(HttpServletRequest request, Model model, Article article, String categoryIds, Page page) throws Exception {
        model.addAttribute("article",article);
        model.addAttribute("categoryIds",categoryIds);
        return "wechat/articles";
    }


    @RequestMapping("/info")
    public String info(HttpServletRequest request, Model model, Integer articleId, String articleContent) throws Exception {
        Article article = articleService.verifyArticle(articleId);
        model.addAttribute(article);
        model.addAttribute("articleContent", articleContent);
        return "wechat/articleInfo";
    }
}
