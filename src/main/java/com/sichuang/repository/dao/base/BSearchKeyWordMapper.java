package com.sichuang.repository.dao.base;

import com.sichuang.repository.model.entity.SearchKeyWordExample;

import java.util.List;

/**
 * @author xs
 * @date 2018/9/4 9:27
 */
public interface BSearchKeyWordMapper {

    /**
     * 批量插入关键字
     * @param keyWordList
     * @return
     */
    int insertKeyWord(List<String> keyWordList);
}
