<!DOCTYPE html>
<html>
<head>
    <base href="${basePath}">
    <meta charset="utf-8">
    <title>${weChatTitle}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link rel="stylesheet" href="mobile/css/jquery.mobile-1.4.5.min.css">
    <link rel="stylesheet" href="${basePath + urls.getForLookupPath('/mobile/css/mobile-style.css')}">


</head>
<body>
<div data-role="page" id="pageone">

    <div data-role="header">
        <form id="searchForm" method="post" action="weChat/article/list" class="ui-filterable"
              data-ajax="false">
            <div class="ui-field-contain">
                <input type="text" data-type="search" name="articleContent" id="search"
                       value="${article.articleContent!}"
                       placeholder="搜索文章">
            </div>
            <div class="ui-field-contain">
                <fieldset data-role="controlgroup" data-type="horizontal">
                <#list categoryList as category>
                    <label for="category_${category.categoryId}">${category.categoryName}</label>
                    <input type="checkbox" name="categoryIdList" id="category_${category.categoryId}"
                           value="${category.categoryId}">
                </#list>
                </fieldset>
            </div>
            <input type="hidden" name="categoryIds">

        </form>
    </div>
    <div data-role="main" class="ui-content">
        <ul data-role="listview" id="article_list" class="m">
            <li data-role="divider">热门搜索</li>
            <#list searchKeyWordList as keyWord>
                <li data-icon="false" class="keyWord ">${keyWord.keyWord}</li>
            </#list>

        </ul>
    </div>
</div>
<!-- jQuery -->
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="mobile/js/jquery.mobile-1.4.5.min.js"></script>

<script>
    $(document).on("pagecreate", "#pageone", function () {
        //选中分类
        var categoryIds = "${categoryIds!}";
        var categoryIdArr = categoryIds.split(",");

        if (categoryIdArr.length > 0) {
            for (var i = 0; i < categoryIdArr.length; i++) {
                console.log(categoryIdArr[i]);
                $("input:checkbox[name='categoryIdList'][value='" + categoryIdArr[i] + "']").attr("checked", true).checkboxradio("refresh");
            }
        }
    });

    $(function () {
        $('#search').focus();

        //获取categoryIds
        $('input:checkbox[name="categoryIdList"]').change(function () {
            var arrs = new Array();
            //获取站内设施搜索选项
            $("input:checkbox[name='categoryIdList']:checked").each(function () {
                arrs.push($(this).val());
            });
            $("input[name='categoryIds']").val(arrs.join(","));
        });


        $('.keyWord').click(function () {
            $('#search').val($(this).text());
        });

    });

</script>
</body>
</html>
