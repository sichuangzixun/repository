package com.sichuang.repository.service;

import com.sichuang.repository.model.entity.SearchKeyWord;

import java.util.List;
import java.util.Map;

/**
 * @author xs
 * @date 2018/9/4 9:24
 */
public interface ISearchKeyWordService {

    /**
     * 保存搜索关键词
     * @param keyWord
     * @return
     */
    Map<String, Object> save(String keyWord);

    /**
     * 获取前5个热门搜素关键词
     * @return
     */
    List<SearchKeyWord> getTopSearch();
}
