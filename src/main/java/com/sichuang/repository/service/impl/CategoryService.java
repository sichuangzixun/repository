package com.sichuang.repository.service.impl;

import com.sichuang.repository.common.MyResponse;
import com.sichuang.repository.common.StringUtil;
import com.sichuang.repository.dao.CategoryMapper;
import com.sichuang.repository.model.entity.Article;
import com.sichuang.repository.model.entity.ArticleExample;
import com.sichuang.repository.model.entity.Category;
import com.sichuang.repository.model.entity.CategoryExample;
import com.sichuang.repository.other.TreeNode;
import com.sichuang.repository.service.IArticleService;
import com.sichuang.repository.service.ICategoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author xs
 * @date 2018/8/21 15:20
 */
@Service
public class CategoryService implements ICategoryService {

    @Resource
    private CategoryMapper categoryMapper;
    @Resource
    private IArticleService articleService;

    @Override
    public ArrayList<TreeNode> getTree(boolean isArticle) {
        int categoryId = 0;
        return getChildrenByParentId(categoryId, isArticle);
    }

    @Override
    public ArrayList<TreeNode> getChildrenByParentId(int categoryId, boolean isArticle) {
        ArrayList<TreeNode> treeNodes = new ArrayList<>();
        //1、获取分类节点
        List<Category> categoryList = getByParentId(categoryId);
        if (categoryList.isEmpty() == false) {
            for (int i = 0; i < categoryList.size(); i++) {
                Category category = categoryList.get(i);
                //设置树节点
                TreeNode treeNode = new TreeNode();
                treeNode.setId(category.getCategoryId());
                treeNode.setText(category.getCategoryName());

                //获取下级节点
                ArrayList<TreeNode> children = getChildrenByParentId(category.getCategoryId(), isArticle);
                if (children.isEmpty() == false) {
                    treeNode.setChildren(children);
                }
                treeNodes.add(treeNode);
            }
        }

        if (isArticle) {
            //2、获取文章节点
            List<Article> articleList = articleService.getByCategoryId(categoryId);
            if (articleList.isEmpty() == false) {
                for (int i = 0; i < articleList.size(); i++) {
                    Article article = articleList.get(i);
                    TreeNode treeNode = new TreeNode();
                    treeNode.setId(article.getArticleId());
                    treeNode.setText(article.getArticleTitle());
                    treeNode.setType("file");
                    treeNodes.add(treeNode);
                }
            }
        }
        return treeNodes;
    }

    @Override
    public List<Category> getByParentId(int categoryId) {
        CategoryExample example = new CategoryExample();
        example.createCriteria().andIsDeleteEqualTo(false).andParentIdEqualTo(categoryId);
        example.setOrderByClause("IF(ISNULL(category_order),1,0),category_order asc");
        return categoryMapper.selectByExample(example);
    }

    @Override
    public Map<String, Object> add(Category category) throws Exception {
        category = verifyData(category);
        category.setCategoryId(null);
        int count = categoryMapper.insertSelective(category);
        if (count != 1) {
            throw new Exception("未知原因！");
        }
        return MyResponse.getSuccessResponse(category);
    }

    @Override
    @Transactional
    public Map<String, Object> update(Category category) throws Exception {
        //验证更新
        Category categoryInfo = verifyCategory(category.getCategoryId());
        if (category.getCategoryOrder() == null) {
            //修改记录验证输入。更新顺序不验证
            category = verifyData(category);
            if(category.getCategoryName().equals(categoryInfo.getCategoryName()) == false){
                articleService.updateCategoryName(category);
            }
        }

        int count = categoryMapper.updateByPrimaryKeySelective(category);
        if (count != 1) {
            throw new Exception("未知原因！");
        }
        return MyResponse.getSuccessResponse(category);
    }

    @Override
    public Map<String, Object> remove(Integer categoryId) throws Exception {
        Category categoryInfo = verifyCategory(categoryId);
        //删除分类
        Category category = new Category();
        category.setCategoryId(categoryId);
        category.setIsDelete(true);
        int count = categoryMapper.updateByPrimaryKeySelective(category);
        if (count != 1) {
            throw new Exception("未知原因！");
        }

        CategoryExample example = new CategoryExample();
        example.createCriteria().andParentIdEqualTo(categoryId)
                .andIsDeleteEqualTo(false);
        //获取全部分类
        List<Category> categoryList = categoryMapper.selectByExample(example);
        List<Integer> categoryIdList = new ArrayList<>();
        categoryIdList.add(categoryId);
        if (categoryList.isEmpty() == false) {
            for (int i = 0; i < categoryList.size(); i++) {
                categoryIdList.add(categoryList.get(i).getCategoryId());
            }
        }

        //删除下级分类
        category.setCategoryId(null);
        count += categoryMapper.updateByExampleSelective(category, example);

        //修改分类相关文章分类为其他
        articleService.updateCategory(categoryIdList);
        return MyResponse.getResponse(count);
    }

    @Override
    public Category verifyCategory(Integer categoryId) throws Exception {
        if (categoryId == null || categoryId <= 0) {
            throw new Exception("分类不存在！");
        }
        Category category = categoryMapper.selectByPrimaryKey(categoryId);
        if (category == null || category.getIsDelete()) {
            throw new Exception("分类不存在！");
        }
        return category;
    }

    @Override
    public Category verifyData(Category category) throws Exception {
        if (StringUtil.isEmpty(category.getCategoryName())) {
            throw new Exception("分类名称不能为空！");
        }
        //验证父级分类
        if (category.getParentId() == null || category.getParentId() <= 0) {
            throw new Exception("请选择父级分类！");
        } else {
            Category categoryParent = verifyCategory(category.getParentId());
            category.setTopCategoryId(categoryParent.getTopCategoryId());
        }

        return category;
    }
}
