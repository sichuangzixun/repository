package com.sichuang.repository.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sichuang.repository.api.Ewaytec2001API;
import com.sichuang.repository.common.MD5Util;
import com.sichuang.repository.common.MyResponse;
import com.sichuang.repository.common.PasswordEncrypt;
import com.sichuang.repository.common.StringUtil;
import com.sichuang.repository.config.BaseConfig;
import com.sichuang.repository.controller.BaseController;
import com.sichuang.repository.dao.UserMapper;
import com.sichuang.repository.model.entity.Page;
import com.sichuang.repository.model.entity.User;
import com.sichuang.repository.model.entity.UserExample;
import com.sichuang.repository.other.EnumManage;
import com.sichuang.repository.service.IUserService;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@Service
public class UserService implements IUserService {

    @Resource
    private Ewaytec2001API ewaytec2001API;
    @Resource
    private UserMapper userMapper;

    @Override
    public Map<String, Object> login(HttpServletRequest request, User user) throws Exception {
        String password = PasswordEncrypt.getPassword(user.getPassword());
        UserExample example = new UserExample();
        example.createCriteria().andLoginNameEqualTo(user.getLoginName())
                .andPasswordEqualTo(password);
        // TODO Auto-generated method stub
        List<User> users = userMapper.selectByExample(example);
        if (users.isEmpty()) {
            throw new Exception("账号或密码错误！");
        }
        BaseController.saveUserSession(request, users.get(0));
        return MyResponse.getResponse(true, users.get(0));
    }

    @Override
    public Map<String, Object> logout(HttpServletRequest request) {
        // TODO Auto-generated method stub
        HttpSession httpSession = request.getSession();
        //清空对应的session
        httpSession.invalidate();
        return MyResponse.getSuccessResponse("您已退出登录");
    }

    @Override
    public Map<String, Object> changePassword(HttpServletRequest request, String oldPassword,
                                              String newPassword) throws Exception {
        // TODO Auto-generated method stub
        User userInfo = BaseController.getUserSession(request);
        userInfo = verifyUser(userInfo.getUserId());
        oldPassword = PasswordEncrypt.getPassword(oldPassword);
        if (userInfo.getPassword().equals(oldPassword) == false) {
            throw new Exception("旧密码输入错误，请重新输入！");
        }
        newPassword = PasswordEncrypt.getPassword(newPassword);
        if (userInfo.getPassword().equals(newPassword)) {
            throw new Exception("新密码与旧密码相同，请重新输入！");
        }
        //更新密码
        User user = new User();
        user.setUserId(userInfo.getUserId());
        user.setPassword(newPassword);
        int count = userMapper.updateByPrimaryKeySelective(user);
        if (count != 1) {
            throw new Exception("未知原因！");
        }
        return MyResponse.getResponse(true, "修改密码成功！");

    }

    @Override
    public Map<String, Object> search(Page page) {
        UserExample example = new UserExample();
        example.setPage(page);
        example.setOrderByClause("user_type asc");
        List<User> users = userMapper.selectByExample(example);
        page.setTotalRecords(userMapper.countByExample(example));
        return MyResponse.getResponse(true, page, users);
    }

    @Override
    public Map<String, Object> add(User user) throws Exception {
        //密码加密
        String password = PasswordEncrypt.getPassword(user.getPassword());
        user.setPassword(password);
        //添加普通用户
        user.setUserType(EnumManage.UserTypeEnum.Normal.getId());
        int result = userMapper.insertSelective(user);
        if (result != 1) {
            throw new Exception();
        }
        return MyResponse.getSuccessResponse(user);

    }


    @Override
    public User verifyUser(Integer userId) throws Exception {
        if (userId == null || userId <= 0) {
            throw new Exception("请选择用户！");
        }
        User user = userMapper.selectByPrimaryKey(userId);
        if (user == null) {
            throw new Exception("用户不存在！");
        }
        return user;
    }

    @Override
    public Map<String, Object> weChatLogin(HttpServletRequest request) throws Exception {
        String sourceString = request.getParameter("source");
        String code = request.getParameter("code");
        if (StringUtil.isEmpty(sourceString) || StringUtil.isEmpty(code)) {
            throw new Exception();
        }
        Integer source = Integer.parseInt(sourceString);
        //获取uid
        String grant_type = "authorization_code";
        String client_id = BaseConfig.PublicKey;
        String timestamp = new DateTime().toString(BaseConfig.DateFormatter);
        String sign = StringUtil.createSign(client_id, BaseConfig.SecretKey, timestamp);
        JSONObject resultJSON = null;

        String result = ewaytec2001API.getAccessToken(grant_type, client_id, sign, timestamp, code, source);
        resultJSON = JSON.parseObject(result);
        //获取用户信息
        String access_token = resultJSON.getString("access_token");
        Integer id = resultJSON.getInteger("uid");
        result = ewaytec2001API.getEmployeeInfo(id, sign, timestamp, access_token);
        resultJSON = JSON.parseObject(result);
        User user = saveWeChatUser(resultJSON);
        if (user == null) {
            throw new Exception();
        }
        BaseController.saveUserSession(request, user);
        return MyResponse.getDefaultSuccessResponse();
    }

    @Override
    public User saveWeChatUser(JSONObject userJSON) throws Exception {
        Integer wechatUserId = userJSON.getInteger("id");
        User user = null;
        if (wechatUserId != null) {
            //获取用户信息
            UserExample example = new UserExample();
            example.createCriteria().andWechatUserIdEqualTo(wechatUserId);
            List<User> users = userMapper.selectByExample(example);

            if (users.isEmpty()) {
                if (userJSON.getInteger("userstatus").equals(1)) {
                    //用户已激活
                    user = new User();
                    user.setUserType(EnumManage.UserTypeEnum.Normal.getId());
                    user.setWechatUserId(wechatUserId);
                    user.setEnterpriseId(userJSON.getInteger("enterpriseid"));
                    user.setEnterpriseName(userJSON.getString("enterprisename"));
                    user.setDepartmentId(userJSON.getInteger("departmentid"));
                    user.setDepartmentName(userJSON.getString("departmentname"));
                    user.setRealName(userJSON.getString("realname"));
                    user.setNickName(userJSON.getString("nickname"));
                    user.setMobile(userJSON.getString("mobile"));
                    user.setPosition(userJSON.getString("position"));
                    user.setBusinessDomain(userJSON.getString("businessdomain"));
                    user.setLoginName(userJSON.getString("realname") + userJSON.getString("mobile"));
                    user.setPassword(MD5Util.MD5("123456"));
                    //保存用户信息
                    add(user);
                }
            } else {
                user = users.get(0);
            }
        }

        return user;
    }

}