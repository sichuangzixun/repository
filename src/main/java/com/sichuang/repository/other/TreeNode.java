package com.sichuang.repository.other;

import java.util.ArrayList;

public class TreeNode {

    /** 分类id  category_id **/
    private int id;
    /** 分类名称  category_name **/
    private String text;
    /** 图标类型  type **/
    private String type;
    /** 下级分类  children **/
    private ArrayList<TreeNode> children;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<TreeNode> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<TreeNode> children) {
        this.children = children;
    }

    public void addNode(TreeNode node) {
        this.children.add(node);
    }

}
