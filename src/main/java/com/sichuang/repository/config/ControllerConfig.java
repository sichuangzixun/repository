package com.sichuang.repository.config;

import com.sichuang.repository.common.MyResponse;
import com.sichuang.repository.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.resource.ResourceUrlProvider;

import javax.servlet.http.HttpServletRequest;

/**
 * 静态资源路径
 *
 * @author xs
 * @date 2018/8/1 9:31
 */
@ControllerAdvice
public class ControllerConfig {
    @Autowired
    ResourceUrlProvider resourceUrlProvider;

    /**
     * 静态资源路径/${版本信息}
     *
     * @return
     */
    @ModelAttribute("urls")
    public ResourceUrlProvider urls() {
        return this.resourceUrlProvider;
    }

    /**
     * 项目URL
     *
     * @param request
     * @return
     */
    @ModelAttribute("basePath")
    public String basePath(HttpServletRequest request) {
        return BaseController.getBasePath(request);
    }


    /**
     * 图片存储路径
     *
     * @param request
     * @return
     */
    @ModelAttribute("urlPath")
    public String urlPath(HttpServletRequest request) {
        return BaseConfig.uploadFolderUrl;
    }

    /**
     * 微信端知识库名称
     * @param request
     * @return
     */
    @ModelAttribute("weChatTitle")
    public String weChatTitle(HttpServletRequest request) {
        return BaseConfig.weChatTitle;
    }

    /**
     * 操作成功代码
     *
     * @param request
     * @return
     */
    @ModelAttribute("successCode")
    public String successCode(HttpServletRequest request) {
        return String.valueOf(MyResponse.getSuccessCode());
    }

    /**
     * 未登录代码
     *
     * @param request
     * @return
     */
    @ModelAttribute("needLoginCode")
    public String needLoginCode(HttpServletRequest request) {
        return String.valueOf(MyResponse.getNeedLoginCode());
    }

}
