package com.sichuang.repository.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class BaseConfig implements WebMvcConfigurer {

    //文件保存的根目录
    public static String fileRootFolder;
    //域名
    public static String domainName;
    public static String uploadFolderUrl = "upload/";
    //幸福南网密钥公钥
    public static String weChatTitle = "供电服务知识库";
    public static String SecretKey = "d12ds%2d0zh";//测试密钥：32ldo2k189
    public static String PublicKey = "12";//测试公钥：8120
    public static String DateFormatter = "yyyy-MM-dd HH:mm:ss";

    /**
     * 上传文件路径
     *
     * @param fileRootFolder
     */
    @Value("${fileRootFolder}")
    public void setPublicCertFilePath(String fileRootFolder) {
        BaseConfig.fileRootFolder = fileRootFolder;
    }

    /**
     * 域名
     * @param domainName
     */
    @Value("${domainName}")
    public void setPublicDomainName(String domainName) {
        BaseConfig.domainName = domainName;
    }

    /**
     * 上传文件访问路径映射到/upload
     *
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/" + uploadFolderUrl + "**").addResourceLocations("file:" + fileRootFolder);
    }

}
