<!DOCTYPE html>
<html>
<head>
    <base href="${basePath}">
    <meta charset="utf-8">
    <title>知识库管理后台系统</title>
    <link href="hplus/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="hplus/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="hplus/css/animate.min.css" rel="stylesheet">
    <link href="hplus/css/style.min862f.css?v=4.1.0" rel="stylesheet">
    <link href="hplus/css/plugins/iCheck/custom.css" rel="stylesheet">
</head>
<body class="gray-bg top-navigation" id="page">

<div id="wrapper">

    <div id="page-wrapper" class="gray-bg">

        <div class="row border-bottom white-bg">
            <div class="container">
                <nav class="navbar navbar-static-top" role="navigation">
                    <div class="navbar-header">
                        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar"
                                data-toggle="collapse"
                                class="navbar-toggle collapsed" type="button">
                            <i class="fa fa-reorder"></i>
                        </button>
                    </div>

                    <div class="navbar-collapse collapse" id="navbar">
                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="web/article/"> 首页</a>
                            </li>
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="web/article/list"> 管理文章 </a>
                            </li>
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="web/category/list"> 管理分类 </a>
                            </li>
                            <li class="active">
                                <a aria-expanded="false" role="button" href="web/suggest/"> 反馈建议 </a>
                            </li>
                        </ul>
                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <form role="form" class="form-inline m-sm" action="web/article/" method="post">
                                    <div class="input-group">
                                        <input type="text" placeholder="搜索标题、正文" name="articleContent" class="form-control">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-primary">搜索</button>
                                        </span>
                                    </div>
                                </form>
                            </li>
                            <li>
                                <a href="javascript:void(0);" onclick="openWindow('添加文章', 'web/article/add');" >
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                                   data-toggle="dropdown"> <i class="fa fa-user"></i> </a>
                                <ul role="menu" class="dropdown-menu">
                                    <li><a href="web/user/changePassword">修改密码</a>
                                    </li>
                                    <li><a href="web/user/logout">退出登录</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>

        <div class="wrapper wrapper-content">
            <div class="container min-height">
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="web/article"> 首页</a></li>
                            <li>
                                <strong>反馈建议</strong>
                            </li>
                        </ol>
                            <form id="searchForm" class="form-inline " role="form" action="web/suggest"
                                  method="post">
                                <div class="form-group">
                                    <label class="control-label">处理状态：</label>
                                    <select name="isProcessed" class="form-control" value="${isProcessed!}">
                                        <option value="">全部</option>
                                        <option value="0">未处理</option>
                                        <option value="1">已处理</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="begin" id="begin" value="${page.begin}"/>
                                    <input type="hidden" name="length" id="length" value="${page.length}"/>
                                    <button type="submit" id="btn_query" class="btn btn-success">搜索</button>
                                </div>
                            </form>

                            <div class="mail-tools tooltip-demo m-sm">
                                <div class="btn-group pull-right">
                                    <button id="btn_last" class="btn btn-white btn-sm">
                                        <i class="fa fa-arrow-left"></i>
                                    </button>
                                    <button id="btn_next" class="btn btn-white btn-sm">
                                        <i class="fa fa-arrow-right"></i>
                                    </button>
                                </div>
                                <button id="btn_refresh" class="btn btn-white btn-sm">
                                    <i class="glyphicon glyphicon-refresh"></i>
                                </button>
                                <button id="btn_remove" class="btn btn-danger btn-sm">
                                    <i class="glyphicon glyphicon-trash"></i> 删除
                                </button>
                            </div>
                            <table class="table table-hover table-mail" id="projectTable">
                                <tbody>
                                <tr class="unread">
                                    <td class="check-mail">
                                        <input type="checkbox" class="i-checks" id="select_all" value="1">
                                    </td>
                                    <td>建议内容</td>
                                    <td>添加人</td>
                                    <td class="">添加时间</td>
                                    <td class="">回复内容</td>
                                    <td>回复人</td>
                                    <td class="">回复时间</td>
                                    <td class=""></td>
                                </tr>
                                <#list data as suggest>
                                    <tr class="">
                                        <td class="check-mail">
                                            <input type="checkbox" class="i-checks" name="suggestId[]"
                                                   value="${suggest.suggestId }">
                                        </td>
                                        <td>
                                            ${suggest.suggestContent}
                                        </td>
                                        <td>
                                            ${suggest.addUserName}
                                        </td>
                                        <td><#if suggest.addTime??>${suggest.addTime?datetime}</#if></td>
                                        <td>
                                            ${suggest.replyMessage}
                                        </td>
                                        <td>
                                            ${suggest.replyUserName}
                                        </td>
                                        <td><#if suggest.replyTime??>${suggest.replyTime?datetime}</#if></td>
                                        <td class="project-actions">
                                            <button onclick="openWindow('回复', 'web/suggest/update?suggestId=${suggest.suggestId }')"
                                               class="btn btn-white btn-sm">
                                                <i class="glyphicon glyphicon-edit"></i>
                                                回复
                                            </button>
                                        </td>
                                    </tr>
                                </#list>
                                </tbody>
                            </table>
                            <span class="pull-left small text-muted m">显示第 ${page.begin+1} 到第 ${page.end} 条记录，总共 ${page.totalRecords} 条记录</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="pull-right">
                &copy; 2018
                <a href="#" target="_blank">思创智汇（广州）科技有限公司</a>
            </div>
        </div>
    </div>
</div>
<div id="small-chat">
    <a class="open-small-chat" onclick="page_top()">
        <i class="glyphicon glyphicon-menu-up"></i>
    </a>
</div>
<script src="hplus/js/jquery.min.js?v=2.1.4"></script>
<script src="hplus/js/bootstrap.min.js?v=3.3.6"></script>
<script src="hplus/js/content.min.js?v=1.0.0"></script>
<script src="${basePath + urls.getForLookupPath('/js/function.js')}"></script>
<script src="hplus/js/plugins/layer/layer.min.js"></script>
<script src="hplus/js/plugins/iCheck/icheck.min.js"></script>
<script>

    $(function () {

        $(".i-checks").iCheck({checkboxClass: "icheckbox_square-green", radioClass: "iradio_square-green",});

        //2.初始化Button的点击事件
        var oButtonInit = new ButtonInit();
        oButtonInit.Init();
    });

    var successCode = "${successCode}";
    var basePath = "${basePath}";

    function getIdSelections() {
        var spCodesTemp = "";
        $("input:checkbox[name='suggestId[]']:checked").each(function (i) {
            if (0 == i) {
                spCodesTemp = $(this).val();
            } else {
                spCodesTemp += ("," + $(this).val());
            }
        });
        return spCodesTemp;
    }

    function go(ids, act, word) {
        if (ids.length == 0) {
            alert("您还没有选择！");
            return;
        }
        if (!confirm(word))
            return;
        $.post("suggest/api/" + act + "?suggestIds=" + ids, "", function (data) {
            responseData(data, basePath);
            if (data.code == successCode) {
                alert(data.msg);
                window.location.reload();
            }
        });
    }

    var ButtonInit = function () {
        var oInit = new Object();
        var postdata = {};
        var totalRecords = '${page.totalRecords}';

        oInit.Init = function () {
            //初始化页面上面的按钮事件
            $('#btn_remove').click(function () {
                var ids = getIdSelections();
                go(ids, "remove", "确认要移除选定的项目吗？");
            });

            $('#btn_refresh').click(function () {
                window.location.reload();
            });

            $('#btn_next').click(function () {
                var length =  parseInt($("#length").val());
                var begin = parseInt($("#begin").val()) + length;
                if (begin >= totalRecords) {
                    alert("当前页面为最后一页！");
                    return false;
                }
                $("#begin").val(begin);
                $("#searchForm").submit();
            });

            $('#btn_last').click(function () {
                var begin = parseInt($("#begin").val());
                var length =  parseInt($("#length").val());
                if (begin == 0 ) {
                    alert("当前页面为第一页！");
                    return false;
                }
                $("#begin").val(begin - length);
                $("#searchForm").submit();
            });

            $('#select_all').on('ifChecked', function (event) {
                $("input").iCheck('check');
            });
            $('#select_all').on('ifUnchecked', function (event) {
                $("input").iCheck('uncheck');
            });

            $('#btn_query').click(function () {
                $("#begin").val(0);
            });
        };

        return oInit;
    };
</script>
<style>
    #projectTable {
        min-width: 1000px;
        padding-bottom: 150px;
    }
</style>
</body>
</html>
