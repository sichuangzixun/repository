package com.sichuang.repository.model.entity;

import java.io.Serializable;

public class User implements Serializable {
    /** 用户账户  user_id **/
    private Integer userId;

    /** 普通用户第三方id。  wechat_user_id **/
    private Integer wechatUserId;

    /** 用户类型：1-系统管理员，2-普通用户  user_type **/
    private Integer userType;

    /** 企业ID  enterprise_id **/
    private Integer enterpriseId;

    /** 企业名称  enterprise_name **/
    private String enterpriseName;

    /** 部门ID  department_id **/
    private Integer departmentId;

    /** 部门名称  department_name **/
    private String departmentName;

    /** 真实姓名  real_name **/
    private String realName;

    /** 昵称  nick_name **/
    private String nickName;

    /** 职位  position **/
    private String position;

    /** 业务领域  business_domain **/
    private String businessDomain;

    /** 手机号码  mobile **/
    private String mobile;

    /** 登录账号  login_name **/
    private String loginName;

    /** 密码。5-20个字符。  password **/
    private String password;

    /**   tableName: user   **/
    private static final long serialVersionUID = 1L;

    /**   用户账户  user_id   **/
    public Integer getUserId() {
        return userId;
    }

    /**   用户账户  user_id   **/
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**   普通用户第三方id。  wechat_user_id   **/
    public Integer getWechatUserId() {
        return wechatUserId;
    }

    /**   普通用户第三方id。  wechat_user_id   **/
    public void setWechatUserId(Integer wechatUserId) {
        this.wechatUserId = wechatUserId;
    }

    /**   用户类型：1-系统管理员，2-普通用户  user_type   **/
    public Integer getUserType() {
        return userType;
    }

    /**   用户类型：1-系统管理员，2-普通用户  user_type   **/
    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    /**   企业ID  enterprise_id   **/
    public Integer getEnterpriseId() {
        return enterpriseId;
    }

    /**   企业ID  enterprise_id   **/
    public void setEnterpriseId(Integer enterpriseId) {
        this.enterpriseId = enterpriseId;
    }

    /**   企业名称  enterprise_name   **/
    public String getEnterpriseName() {
        return enterpriseName;
    }

    /**   企业名称  enterprise_name   **/
    public void setEnterpriseName(String enterpriseName) {
        this.enterpriseName = enterpriseName == null ? null : enterpriseName.trim();
    }

    /**   部门ID  department_id   **/
    public Integer getDepartmentId() {
        return departmentId;
    }

    /**   部门ID  department_id   **/
    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    /**   部门名称  department_name   **/
    public String getDepartmentName() {
        return departmentName;
    }

    /**   部门名称  department_name   **/
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName == null ? null : departmentName.trim();
    }

    /**   真实姓名  real_name   **/
    public String getRealName() {
        return realName;
    }

    /**   真实姓名  real_name   **/
    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }

    /**   昵称  nick_name   **/
    public String getNickName() {
        return nickName;
    }

    /**   昵称  nick_name   **/
    public void setNickName(String nickName) {
        this.nickName = nickName == null ? null : nickName.trim();
    }

    /**   职位  position   **/
    public String getPosition() {
        return position;
    }

    /**   职位  position   **/
    public void setPosition(String position) {
        this.position = position == null ? null : position.trim();
    }

    /**   业务领域  business_domain   **/
    public String getBusinessDomain() {
        return businessDomain;
    }

    /**   业务领域  business_domain   **/
    public void setBusinessDomain(String businessDomain) {
        this.businessDomain = businessDomain == null ? null : businessDomain.trim();
    }

    /**   手机号码  mobile   **/
    public String getMobile() {
        return mobile;
    }

    /**   手机号码  mobile   **/
    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    /**   登录账号  login_name   **/
    public String getLoginName() {
        return loginName;
    }

    /**   登录账号  login_name   **/
    public void setLoginName(String loginName) {
        this.loginName = loginName == null ? null : loginName.trim();
    }

    /**   密码。5-20个字符。  password   **/
    public String getPassword() {
        return password;
    }

    /**   密码。5-20个字符。  password   **/
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }
}