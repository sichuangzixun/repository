package com.sichuang.repository.model.entity;

import java.io.Serializable;

public class SearchKeyWord implements Serializable {
    /** 搜索关键字  key_word **/
    private String keyWord;

    /** 搜索次数  search_times **/
    private Integer searchTimes;

    /**   tableName: search_key_word   **/
    private static final long serialVersionUID = 1L;

    /**   搜索关键字  key_word   **/
    public String getKeyWord() {
        return keyWord;
    }

    /**   搜索关键字  key_word   **/
    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord == null ? null : keyWord.trim();
    }

    /**   搜索次数  search_times   **/
    public Integer getSearchTimes() {
        return searchTimes;
    }

    /**   搜索次数  search_times   **/
    public void setSearchTimes(Integer searchTimes) {
        this.searchTimes = searchTimes;
    }
}