package com.sichuang.repository.service.impl;

import com.sichuang.repository.common.MyResponse;
import com.sichuang.repository.common.StringUtil;
import com.sichuang.repository.dao.SearchKeyWordMapper;
import com.sichuang.repository.dao.base.BSearchKeyWordMapper;
import com.sichuang.repository.model.entity.Page;
import com.sichuang.repository.model.entity.SearchKeyWord;
import com.sichuang.repository.model.entity.SearchKeyWordExample;
import com.sichuang.repository.service.ISearchKeyWordService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author xs
 * @date 2018/9/4 9:34
 */
@Service
public class SearchKeyWordService implements ISearchKeyWordService {

    @Resource
    private SearchKeyWordMapper searchKeyWordMapper;
    @Resource
    private BSearchKeyWordMapper bSearchKeyWordMapper;

    @Override
    public Map<String, Object> save(String keyWord) {
        if(StringUtil.isEmpty(keyWord)){
            return MyResponse.getDefaultSuccessResponse();
        }
        List<String> keyWordList = StringUtil.parseStringList(keyWord, " ");
        int count = bSearchKeyWordMapper.insertKeyWord(keyWordList);
        return MyResponse.getResponse(count);
    }

    @Override
    public List<SearchKeyWord> getTopSearch() {
        SearchKeyWordExample example = new SearchKeyWordExample();
        example.setLimit(5);
        example.setOrderByClause("search_times desc");
        return searchKeyWordMapper.selectByExample(example);
    }
}
