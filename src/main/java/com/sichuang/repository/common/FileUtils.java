package com.sichuang.repository.common;

import com.sichuang.repository.config.BaseConfig;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

public class FileUtils {

    /**
     * 保存文件
     *
     * @param path     文件路径
     * @param fileData 文件
     * @return 返回全路径
     */
    public static String saveFile(String path, MultipartFile fileData) {
        if (path == null || path.isEmpty()) {
            return "";
        }
        /* 构建文件目录 */
        File fileDir = new File(path.substring(0, path.lastIndexOf("/")));
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }
        try {
            FileOutputStream out = new FileOutputStream(path);
            // 写入文件
            out.write(fileData.getBytes());
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
        return path;
    }

    /**
     * @param rootPath 根目录
     * @param sonPath  子目录，包含后缀
     * @param fileData
     * @return 返回[全路径, 子路径]
     */
    public static String[] saveFile(String rootPath, String sonPath, MultipartFile fileData) {
        String[] strings = new String[2];
        if (rootPath == null || rootPath.isEmpty() || sonPath == null || sonPath.isEmpty()) {
            return strings;
        }
        String rootUrl = saveFile(rootPath + sonPath, fileData);
        strings[0] = rootUrl;
        strings[1] = sonPath;
        return strings;
    }

    /**
     * @param rootPath 根目录
     * @param segments 子目录，不包含后缀
     * @param suffix   后缀 如.exe/.png等
     * @param fileData
     * @return 返回[全路径, 子路径]
     */
    public static String[] saveFile(String rootPath, String segments, String suffix, MultipartFile fileData) {
        String[] strings = new String[2];
        if (rootPath == null || rootPath.isEmpty() || segments == null || segments.isEmpty() || suffix == null || suffix.isEmpty()) {
            return strings;
        }
        String sonPath = segments + suffix;
        String rootUrl = saveFile(rootPath + sonPath, fileData);
        strings[0] = rootUrl;
        strings[1] = sonPath;
        return strings;
    }

    /**
     * @param rootPath 根目录
     * @param segments 子目录，不包含后缀
     * @param fileData
     * @return 返回[全路径, 子路径]
     */
    public static String[] saveJpegImage(String rootPath, String segments, MultipartFile fileData) {
        String[] strings = new String[2];
        if (rootPath == null || rootPath.isEmpty() || segments == null || segments.isEmpty()) {
            return strings;
        }
        String sonPath = segments + ".jpeg";
        String rootUrl = saveFile(rootPath + sonPath, fileData);
        strings[0] = rootUrl;
        strings[1] = sonPath;
        return strings;
    }


    /**
     * 删除文件
     *
     * @param pathUrl
     * @return
     */
    public static Map<String, Object> deleteFile(String pathUrl) {
        File file = new File(BaseConfig.fileRootFolder + pathUrl);
        // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
        if (file.exists() && file.isFile()) {
            if (file.delete()) {
                return MyResponse.getDefaultSuccessResponse();
            } else {
                return MyResponse.getErrorResponse("删除失败！");
            }
        } else {
            return MyResponse.getErrorResponse("文件不存在！");
        }
    }

    public static Map<String, Object> deleteFile(String rootPathUrl, String sonRootPathUrl) {
        return deleteFile(rootPathUrl + sonRootPathUrl);
    }

    /**
     * web上传文件
     *
     * @param request
     * @param fileName 文件名
     * @param pathUrl  相对路径
     * @return
     */
    public static Map<String, Object> pngUpload(HttpServletRequest request, String fileName, String pathUrl) {

        ArrayList<String> newFilePath = new ArrayList<>();
        int index = 0;

        // 根据配置文件获取服务器图片存放路径
        String url = BaseConfig.fileRootFolder + pathUrl;

        /* 构建文件目录 */
        File fileDir = new File(url);
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }

        MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
        Iterator<String> iter = multiRequest.getFileNames();

        while (iter.hasNext()) {
            MultipartFile file = multiRequest.getFile(iter.next());
            if (file != null && file.getSize() > 0) {
                String oldFileName = file.getOriginalFilename();
                String suffixName = oldFileName.substring(oldFileName.lastIndexOf("."));
                if (suffixName.trim().equals(".exe")) {
                    continue;
                }

                String newFileName = fileName + "_" + new Date().getTime() + "_" + index + ".png";

                String newFile = Paths.get(url, newFileName).toString();
                try {
                    // 将上传文件写到服务器上指定的文件
                    file.transferTo(new File(newFile));
                    newFilePath.add(pathUrl + newFileName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                index++;
            }
        }


        if (index == 0) {
            return MyResponse.getDefaultSuccessResponse();
        }

        if (newFilePath.isEmpty() == true) {
            return MyResponse.getResponse(false, "上传文件失败！");
        }

        if (newFilePath.size() == index) {
            return MyResponse.getResponse(true, newFilePath);
        }

        return MyResponse.getDefaultErrorResponse();
    }

}
