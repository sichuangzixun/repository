package com.sichuang.repository.model.entity;

import java.io.Serializable;

public class Category implements Serializable {
    /** 分类id  category_id **/
    private Integer categoryId;

    /** 分类名称。不能为空。  category_name **/
    private String categoryName;

    /** 删除状态。不能为空。默认未删除0。  is_delete **/
    private Boolean isDelete;

    /** 次序，默认为空，排在最后。按数字从小到大排列。  category_order **/
    private Integer categoryOrder;

    /** 父级分类id  parent_id **/
    private Integer parentId;

    /** 一级分类id  top_category_id **/
    private Integer topCategoryId;

    /**   tableName: category   **/
    private static final long serialVersionUID = 1L;

    /**   分类id  category_id   **/
    public Integer getCategoryId() {
        return categoryId;
    }

    /**   分类id  category_id   **/
    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    /**   分类名称。不能为空。  category_name   **/
    public String getCategoryName() {
        return categoryName;
    }

    /**   分类名称。不能为空。  category_name   **/
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName == null ? null : categoryName.trim();
    }

    /**   删除状态。不能为空。默认未删除0。  is_delete   **/
    public Boolean getIsDelete() {
        return isDelete;
    }

    /**   删除状态。不能为空。默认未删除0。  is_delete   **/
    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    /**   次序，默认为空，排在最后。按数字从小到大排列。  category_order   **/
    public Integer getCategoryOrder() {
        return categoryOrder;
    }

    /**   次序，默认为空，排在最后。按数字从小到大排列。  category_order   **/
    public void setCategoryOrder(Integer categoryOrder) {
        this.categoryOrder = categoryOrder;
    }

    /**   父级分类id  parent_id   **/
    public Integer getParentId() {
        return parentId;
    }

    /**   父级分类id  parent_id   **/
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    /**   一级分类id  top_category_id   **/
    public Integer getTopCategoryId() {
        return topCategoryId;
    }

    /**   一级分类id  top_category_id   **/
    public void setTopCategoryId(Integer topCategoryId) {
        this.topCategoryId = topCategoryId;
    }
}