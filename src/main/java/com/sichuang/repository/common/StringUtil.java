package com.sichuang.repository.common;

import java.util.ArrayList;
import java.util.List;

public class StringUtil {

    public static boolean isEmpty(String string) {
        if (string == null || string.trim().isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * 解析ids字符串
     *
     * @param ids
     * @return
     */
    public static List<Integer> parseIdList(String ids) {
        List<Integer> idList = new ArrayList<>();
        if (StringUtil.isEmpty(ids) == false) {
            String[] idStrings = ids.split(",");
            for (int i = 0; i < idStrings.length; i++) {
                if (StringUtil.isEmpty(idStrings[i]) == false) {
                    try {
                        int id = Integer.parseInt(idStrings[i]);
                        if (id > 0) {
                            //未使用权限组可以删除
                            idList.add(id);
                        }
                    } catch (Exception e) {
                        idList.clear();
                        return idList;
                    }
                }
            }
        }
        return idList;
    }

    /**
     * 解析逗号拼接字符串
     *
     * @param strings
     * @return
     */
    public static List<String> parseStringList(String strings, String spilt) {
        if (StringUtil.isEmpty(spilt)) {
            spilt = ",";
        }
        List<String> idList = new ArrayList<>();
        if (StringUtil.isEmpty(strings) == false) {
            String[] idStrings = strings.split(spilt);
            for (int i = 0; i < idStrings.length; i++) {
                idList.add(idStrings[i]);
            }
        }
        return idList;
    }

    //从html中提取纯文本
    public static String StripHTML(String strHtml) {
        String content = strHtml.replaceAll("</?[^>]+>", ""); //剔出<html>的标签
        content = content.replaceAll("<a>\\s*|\t|\r|\n</a>", "");//去除字符串中的空格,回车,换行符,制表符
        return content;
    }

    /**
     * 幸福南网生成签名时
     * MD5（client_id=消费者公钥client_key=消费者秘钥timestamp=当前时间戳）
     *
     * @param client_id
     * @param client_key
     * @param timestamp
     * @return
     */
    public static String createSign(String client_id, String client_key, String timestamp) {
        String sign = "client_id=" + client_id + "client_key=" + client_key + "timestamp=" + timestamp;
        return MD5Util.MD5(sign);
    }
}
